export const environment = {
  name: 'local',
  production: false,
  surveyApiUrl: 'http://localhost:8080/api',
  semanticApiUrl: 'http://localhost:8000',
}
