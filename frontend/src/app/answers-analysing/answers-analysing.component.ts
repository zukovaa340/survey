import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, Subject, takeUntil} from "rxjs";
import {Survey} from "../model/survey";
import {ActivatedRoute, Router} from "@angular/router";
import {SurveyService} from "../service/survey.service";
import {QuestionType} from "../model/question-type";

@Component({
  selector: 'app-answers-analysing',
  templateUrl: './answers-analysing.component.html',
  styleUrls: ['./answers-analysing.component.css']
})
export class AnswersAnalysingComponent implements OnDestroy, OnInit {
  protected readonly QuestionType = QuestionType;
  private ngUnsubscribe = new Subject<void>();

  surveyId: number;
  survey: Survey;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private surveyService: SurveyService) {
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe(params => {
      this.surveyId = params["id"];

      if (!this.surveyId) {
        this.router.navigate(['/']);
      }
      this.surveyService.get(this.surveyId, true).pipe(map(
        survey => {
          survey.questions.forEach(question => {
            question.userAnswers.forEach(userAnswer => {
              userAnswer.answerDate = new Date(userAnswer.answerDate ?? '');
            })
          });
          return survey;
        }
      )).subscribe((survey: Survey) => {
        this.survey = survey;
      });
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
