import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartConfiguration} from "chart.js";
import {Question} from "../../model/question";
import {AnswerOption} from "../../model/answer-option";
import {QuestionType} from "../../model/question-type";
import {NgChartsModule} from "ng2-charts";
import {UserAnswer} from "../../model/user-answer";
import ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-question-bar-chart',
  standalone: true,
  imports: [CommonModule, NgChartsModule],
  templateUrl: './question-bar-chart.component.html',
  styleUrls: ['./question-bar-chart.component.css']
})
export class QuestionBarChartComponent {

  @Input() set question(question: Question) {
    this._question = question;
    this.updateChartData();
  }

  get question(): Question {
    return this._question;
  }

  public plugin = [ChartDataLabels];
  public barChartType = 'bar' as const;
  public barChartData: ChartConfiguration<'bar'>['data'];
  public barChartOptions: ChartConfiguration<'bar'>['options'] = {
    responsive: true,
    indexAxis: 'y',
    scales: {
      y: {
        grid: {
          display: false,
        },
        ticks: {
          display: false,
        },
      }
    },
    plugins: {
      legend: {
        display: false,
      },
      datalabels: {
        color: '#000000',
        anchor: 'center',
        align: 'top',
        offset: 15,
        formatter: function (value, context) {
          const a = context.chart.data.labels;
          return a ? a[context.dataIndex] : '';
        },
        display: function (context) {
          const a = context.dataset.data[context.dataIndex];
          return typeof a === 'number' ? a > 0 : false;
        }
      }
    },
  };

  private _question: Question;


  private updateChartData(): void {
    const labels: string[] = [];
    const data: number[] = [];

    this.question.answerOptions.forEach((answerOption: AnswerOption) => {
      labels.push(answerOption.title!);
      data.push(this.countAnswers(answerOption));
    });

    this.barChartData = {
      labels: labels,
      datasets: [{
        backgroundColor: '#3B3E91',
        maxBarThickness: 30,
        categoryPercentage: 1,
        data: data,
      }]
    };
  }

  private countAnswers(answerOption: AnswerOption): number {
    let count = 0;

    this.question.userAnswers.forEach((userAnswer: UserAnswer) => {
      if (this.question.type === QuestionType.MULTIPLE_CHOICE) {
        if (answerOption?.id && userAnswer?.answerOptionIds?.includes(answerOption.id)) {
          count++;
        }
      } else if (this.question.type === QuestionType.SINGLE_CHOICE) {
        if (userAnswer.answerOptionId === answerOption.id) {
          count++;
        }
      }
    });

    return count;
  }

}
