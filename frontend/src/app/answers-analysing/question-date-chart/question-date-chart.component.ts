import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Question} from "../../model/question";
import {UserAnswer} from "../../model/user-answer";
import {NgChartsModule} from "ng2-charts";
import {ChartConfiguration} from "chart.js";
import 'chartjs-adapter-date-fns';
import { format } from 'date-fns';

@Component({
  selector: 'app-question-date-chart',
  standalone: true,
  imports: [CommonModule, NgChartsModule],
  templateUrl: './question-date-chart.component.html',
  styleUrls: ['./question-date-chart.component.css']
})
export class QuestionDateChartComponent {
  @Input() set question(question: Question) {
    this._question = question;
    this.updateChartData();
  }

  get question(): Question {
    return this._question;
  }

  private _question: Question;
  public lineChartType = 'line' as const;
  public lineChartData: ChartConfiguration<'line'>['data'];
  public lineChartOptions: ChartConfiguration<'line'>['options'] = {
    responsive: true,
    scales: {
      x: {

      },
    },
    plugins: {
      legend: {
        display: false,
      },
    },
  };


  private updateChartData(): void {
    const dateCounts: { [key: string]: number } = {};
    this.question.userAnswers.forEach((userAnswer: UserAnswer) => {
      const date: Date | undefined = userAnswer.answerDate;

      if (date) {
        const dateString = format(date, 'dd.MM.yyyy');
        if (dateCounts[dateString]) {
          dateCounts[dateString]++;
        } else {
          dateCounts[dateString] = 1;
        }
      }
    });

    const labels: string[] = Object.keys(dateCounts);
    const data: number[] = Object.values(dateCounts);
    console.log(labels, data)

    this.lineChartData = {
      labels: labels,
      datasets: [{
        label: 'Answers count',
        data: data,
        fill: false,
        borderColor: '#3B3E91'
      }]
    };
  }


}
