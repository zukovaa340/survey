import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {MatChipsModule} from "@angular/material/chips";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {InputSearchComponent} from "../../input-search/input-search.component";
import {PunctuationRegex, StopWords} from "./stop-words";
import {Question} from "../../model/question";

@Component({
  selector: 'app-text-input-analyse-card',
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatChipsModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    InputSearchComponent,
  ],
  templateUrl: './text-input-analyse-card.component.html',
  styleUrls: ['./text-input-analyse-card.component.css']
})
export class TextInputAnalyseCardComponent {
  mostUsedWords: string[] = [];
  answers: string[] = [];//= ['Це поле є первинним ключем в таблиці бази даних. Значення цього поля генерується автоматично.', 'Це поле відповідає заголовку опитування. Воно відповідає стовпцю "title" в таблиці, не може бути null і повинно бути унікальним. Довжина заголовку повинна бути від 5 до 200 символів.', 'Це поле відповідає автору опитування. Воно вказує на відношення "багато до одного" з класом User.', 'В космосі приємніше', 'В огороді', 'Ваш клас Survey вже добре структурований, але я можу додати коментарі для кращого розуміння. Ось коментарі до кожного поля вашого класу Survey']
  filteredAnswers: string[] = []; //= [...this.answers];
  searchValue: string = '';

  @Input() set question(question: Question) {
    this._question = question;
    this.answers = question.userAnswers.map(answer => answer.answerText || '').filter(answer => answer !== '');
    this.mostUsedWords = this.getMostFrequentWords(this.answers);
    this.filteredAnswers = [...this.answers];
  }

  get question(): Question {
    return this._question;
  }

  private _question: Question;

  onSearch(searchValue: string): void {
    const searchValueLowerCase = searchValue.toLowerCase();
    this.searchValue = searchValue;
    this.filteredAnswers = this.answers.filter(answer => answer.toLowerCase().includes(searchValueLowerCase));
  }

  getMostFrequentWords(answers: string[]): string[] {
    // Clean the answers by removing punctuation marks and stop words
    const cleanedAnswers = answers.map(sentence =>
      sentence.toLowerCase()
        .replace(PunctuationRegex, "")
        .split(/\s+/)
        .filter(word => !StopWords.includes(word)));

    // Create a frequency map
    const frequencyMap = cleanedAnswers.reduce((map, words) => {
      words.forEach(word => map[word] = (map[word] || 0) + 1);
      return map;
    }, {} as Record<string, number>);

    // Getting the array in descending order based on the frequency
    const frequencyPairs = Object.entries(frequencyMap).sort((a, b) => b[1] - a[1]);

    // Get the top 5 frequent words
    return frequencyPairs.slice(0, 5).map(pair => pair[0]);
  }

}

