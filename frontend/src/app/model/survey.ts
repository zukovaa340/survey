import { Question } from "./question";

export interface Survey {
  title: string,
  questions: Question[],
  id?: number,
  authorId?: number
}


