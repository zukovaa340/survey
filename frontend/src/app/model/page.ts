export class Page<T> {
  constructor(public content: T,
              public currentPage: number,
              public totalItems: number,
              public totalPages: number) {

  }
}
