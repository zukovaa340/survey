export enum Role {
  MODERATOR = 'MODERATOR',
  USER = 'USER',
  ADMIN = 'ADMIN',
}
