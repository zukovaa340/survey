export interface AnswerOption {
  order?: number,
  id?: number,
  title?: string
}
