export interface UserAnswer {
    answerText?: string;
    answerDate?: Date;
    answerOptionId?: number;
    answerOptionIds?: number[];
    questionId: number;
}
