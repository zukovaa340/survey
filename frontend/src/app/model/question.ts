import { QuestionType } from "./question-type";
import { AnswerOption } from "./answer-option";
import {UserAnswer} from "./user-answer";

export class Question {
  constructor(public answerOptions: AnswerOption[],
              public userAnswers: UserAnswer[],
              public id?: number,
              public title?: string,
              public type?: QuestionType,
              public sentimentPrediction?: number) {

  }
}
