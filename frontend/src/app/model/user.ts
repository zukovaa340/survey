import { Role } from "./role";
import { Status } from "./status";

export class User {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  role?: Role;
  status?: Status;

  constructor(
    id?: number,
    firstName?: string,
    lastName?: string,
    email?: string,
    phone?: string,
    role?: Role,
    status?: Status
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.role = role;
    this.status = status;
  }

}
