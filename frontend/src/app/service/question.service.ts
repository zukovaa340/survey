import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Question} from "../model/question";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private apiUrl: string = environment.surveyApiUrl + '/questions';

  constructor(private http: HttpClient) {
  }

  patch(id: number, data: Partial<Question>): Observable<Question> {
    return this.http.patch<Question>(`${this.apiUrl}/${id}`, data);
  }

}
