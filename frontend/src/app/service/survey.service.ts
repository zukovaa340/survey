import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Survey} from "../model/survey";
import {Page} from "../model/page";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  private apiUrl: string = environment.surveyApiUrl + '/surveys';

  constructor(private http: HttpClient) {
  }

  getAll(page?: number, size?: number, title?: string): Observable<Page<Survey[]>> {
    let params: HttpParams = new HttpParams()
    if (title) {
      params = params.append(`title`, title)
    }
    if (page) {
      params = params.append(`page`, page);
    }
    if (size) {
      params = params.append(`size`, size);
    }

    return this.http.get<Page<Survey[]>>(`${this.apiUrl}`, {params});
  }

  get(id: number, fetchAnswers?: boolean): Observable<Survey> {
    let params: HttpParams = new HttpParams();
    if (fetchAnswers) {
      params = params.append('fetchAnswers', String(fetchAnswers));
    }
    return this.http.get<Survey>(`${this.apiUrl}/${id}`, {params});
  }

  getMaxQuestionsInSurveys(): Observable<number> {
    return this.http.get<number>(`${this.apiUrl}/max-questions`);
  }

  create(data: Survey): Observable<Survey> {
    return this.http.post<Survey>(this.apiUrl, data);
  }

  update(id: number, data: Survey): Observable<Survey> {
    return this.http.put<Survey>(`${this.apiUrl}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
