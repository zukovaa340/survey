import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import {map, Observable} from "rxjs";
import { Page } from "../model/page";
import { environment } from "../../environments/environment";
import { UserAnswer } from "../model/user-answer";

@Injectable({
  providedIn: 'root'
})
export class UserAnswerService {

  constructor(private http: HttpClient) {
  }

  getAll(surveyId: number, questionId: number, page?: number, size?: number): Observable<Page<UserAnswer[]>> {
    const apiUrl = this.getApiUrl(surveyId, questionId);
    let params: HttpParams = new HttpParams()
    if (page) {
      params = params.append(`page`, page);
    }
    if (size) {
      params = params.append(`size`, size);
    }

    return this.http.get<Page<UserAnswer[]>>(`${apiUrl}`, { params });
  }

  getAllBySurveyId(surveyId: number): Observable<UserAnswer[]> {
    const apiUrl = this.getApiUrl(surveyId);
    return this.http.get<UserAnswer[]>(`${apiUrl}`);
  }

  getAllByQuestionId(surveyId: number, questionId: number): Observable<UserAnswer[]> {
    const apiUrl = this.getApiUrl(surveyId, questionId);
    return this.http.get<UserAnswer[]>(`${apiUrl}`);
  }

  get(surveyId: number, questionId: number, id: number): Observable<UserAnswer> {
    const apiUrl = this.getApiUrl(surveyId, questionId);

    return this.http.get<UserAnswer>(`${apiUrl}/${id}`).pipe(
      map(userAnswer => {
        userAnswer.answerDate = new Date(userAnswer.answerDate ?? '');
        return userAnswer;
      })
    );
  }

  create(surveyId: number, userAnswers: UserAnswer[]): Observable<UserAnswer[]> {
    const apiUrl = this.getApiUrl(surveyId);

    return this.http.post<UserAnswer[]>(`${apiUrl}`, userAnswers);
  }

  update(surveyId: number, questionId: number, id: number, userAnswer: UserAnswer): Observable<UserAnswer> {
    const apiUrl = this.getApiUrl(surveyId, questionId);

    return this.http.put<UserAnswer>(`${apiUrl}/${id}`, userAnswer).pipe(
      map(userAnswer => {
        userAnswer.answerDate = new Date(userAnswer.answerDate ?? '');
        return userAnswer;
      })
    );
  }

  delete(surveyId: number, questionId: number, id: number): Observable<void> {
    const apiUrl = this.getApiUrl(surveyId, questionId);

    return this.http.delete<void>(`${apiUrl}/${id}`);
  }

  private getApiUrl(surveyId: number, questionId?: number): string {
    return questionId
      ? `${environment.surveyApiUrl}/surveys/${surveyId}/questions/${questionId}/user-answers`
      : `${environment.surveyApiUrl}/surveys/${surveyId}/user-answers`

  }

}
