import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from "../model/user";
import { Page } from "../model/page";


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly API_URL: string = environment.surveyApiUrl + '/users';

  constructor(private http: HttpClient) {
  }

  getUsers(page?: number): Observable<Page<User[]>> {
    let params: HttpParams = new HttpParams()

    if (page) {
      params = params.append(`page`, page);
    }

    return this.http.get<Page<User[]>>(`${this.API_URL}`, { params });
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.API_URL}/${user.id}`, user);
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.API_URL}`, user);
  }

  signUp(user: User): Observable<User> {
    return this.http.post<User>(`${this.API_URL}/signup`, user);
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.API_URL}/${id}`);
  }
}
