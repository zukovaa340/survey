import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from "../model/user";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly USER_STORAGE_KEY: string = 'user';
  private readonly API_URL: string = environment.surveyApiUrl + '/auth';

  constructor(
    private http: HttpClient,
    private route: Router
  ) {
  }

  get user(): User | undefined {
    let user = localStorage.getItem(this.USER_STORAGE_KEY);
    if (user) {
      return JSON.parse(user);
    }
    return undefined;
  }

  signIn(email: string, password: string): Observable<User> {
    return this.http.post<User>(this.API_URL + '/login', {
      email,
      password
    });
  }

  refreshToken(): Observable<User> {
    return this.http.post<User>(this.API_URL + '/refresh', null);
  }

  signOut(): Observable<void> {
    localStorage.removeItem(this.USER_STORAGE_KEY);

    return this.http.post<void>(this.API_URL + '/logout', null);
  }

  updateUserLocalStorage(user: User): void {
    localStorage.setItem(this.USER_STORAGE_KEY, JSON.stringify(user));
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem(this.USER_STORAGE_KEY);
  }
}
