import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SemanticService {
  private readonly API_URL: string = environment.semanticApiUrl;

  constructor(private http: HttpClient) {
  }

  getPositivePercentage(texts: string[]): Observable<number> {
    const url = `${this.API_URL}/predict`;

    return this.http.post<number>(url, { texts: texts });
  }

}
