import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { ViewSurveysComponent } from "./view-surveys/view-surveys.component";
import { EditSurveysComponent } from "./edit-surveys/edit-surveys.component";
import { CreateSurveysComponent } from "./create-surveys/create-surveys.component";
import { AnalysingSurveysComponent } from "./analysing-surveys/analysing-surveys.component";
import { AnswerSurveyComponent } from "./answer-survey/answer-survey.component";
import { AnswersAnalysingComponent } from "./answers-analysing/answers-analysing.component";
import { IsAuthenticatedGuard } from "./guard/AuthGuard";


const routes: Routes = [
  { path: "", redirectTo: "/surveys", pathMatch: 'full' },
  { path: "surveys", component: ViewSurveysComponent },
  { path: "surveys/:id", component: EditSurveysComponent, canActivate: [IsAuthenticatedGuard] },
  { path: "surveys/:id/answers", component: AnswerSurveyComponent, canActivate: [IsAuthenticatedGuard] },
  { path: "create-surveys", component: CreateSurveysComponent, canActivate: [IsAuthenticatedGuard] },
  { path: "analysing-surveys", component: AnalysingSurveysComponent, canActivate: [IsAuthenticatedGuard] },
  { path: "surveys/:id/analysing-answers", component: AnswersAnalysingComponent, canActivate: [IsAuthenticatedGuard] },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
