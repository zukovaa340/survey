import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, switchMap, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from "../../environments/environment";
import { AuthService } from "../service/auth.service";

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  apiUrl: string = environment.surveyApiUrl;

  constructor(private authService: AuthService,
              private route: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith(this.apiUrl)) {
      req = req.clone({
        withCredentials: true,
      });
    }

    return next.handle(req).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return this.handle401Error(req, next, error);
        }
        return throwError(() => error);
      })
    );
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler, originalError: HttpErrorResponse) {
    return this.authService.refreshToken().pipe(
      switchMap(() => {
        return next.handle(request);
      }),
      catchError(err => {
        console.log(err);
        this.route.navigate(['/surveys']);
        return next.handle(request);
      })
    );
  }
}
