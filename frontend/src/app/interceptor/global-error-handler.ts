import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private snackBar: MatSnackBar) {
  }

  handleError(error: any): void {
    let message: string = 'An unexpected error occurred, please try again later';
    if (error instanceof HttpErrorResponse) {
      switch (error.status) {
        case 400:
          message = 'Provided data invalid. Please check the data and try again';
          break;
        case 401:
          message = 'The request requires user authentication. Please sign in and try again';
          break;
        case 403:
          message = 'You do not have the necessary permissions';
          break;
      }
    }

    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: ['snackbar-error']
    });
    console.error(error);
  }
}
