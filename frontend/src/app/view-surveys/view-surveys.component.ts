import { Component, OnInit } from '@angular/core';
import { SurveyService } from "../service/survey.service";
import { Survey } from "../model/survey";
import { Page } from "../model/page";
import { PageEvent } from "@angular/material/paginator";
import { AuthService } from "../service/auth.service";
import { Role } from "../model/role";
import {MatDialog} from "@angular/material/dialog";
import {DeleteConfirmationDialogComponent} from "../deleteconfirmationdialog/delete-confirmation-dialog.component";

@Component({
  selector: 'app-view-surveys',
  templateUrl: './view-surveys.component.html',
  styleUrls: ['./view-surveys.component.css']
})
export class ViewSurveysComponent implements OnInit {
  public searchKey?: string;
  public surveys?: Page<Survey[]>;
  public pageSize: number = 5;

  constructor(public surveyService: SurveyService,
              public authService: AuthService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.initPageWithoutParams(surveys => this.surveys = surveys);
  }

  initPageWithoutParams(callback: (surveys: Page<Survey[]>) => void): void {
    this.surveyService.getAll().subscribe(callback);
  }

  initPage($event: PageEvent): void {
    this.surveyService.getAll($event.pageIndex, this.pageSize, this.searchKey).subscribe(surveys => {
      this.surveys = surveys;
    });
  }

  initPageByTitle(title: string): void {
    this.surveyService.getAll(undefined, undefined, title).subscribe(surveys => {
      this.surveys = surveys;
      this.searchKey = title;
    });
  }

  resetSearching(): void {
    this.initPageWithoutParams(surveys => {
      this.surveys = surveys;
    });
  }

  deleteSurvey(id?: number): void {
    if (!id) throw new Error('id cant be null')

    this.surveyService.delete(id).subscribe(() => {
      this.initPageWithoutParams(surveys => {
        this.surveys = surveys;
      });
    });

  }

  showDeleteDialog(survey: Survey): void {
    const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {});

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteSurvey(survey.id);
      }
    });
  }

  isAuthor(survey: Survey): boolean {
    const currUser = this.authService?.user;
    if (currUser?.role === Role.ADMIN) return true;

    return Boolean(currUser?.id && currUser.id === survey?.authorId);
  }

}
