import { Component } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {
  opened = true;
  icon = "keyboard_arrow_left";

  onClick() {
    this.opened = !this.opened;
    if (this.opened) {
      this.icon = "keyboard_arrow_left";
    } else {
      this.icon = "keyboard_arrow_right";
    }
  }

}
