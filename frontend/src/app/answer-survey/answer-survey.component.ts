import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserAnswerService} from "../service/user-answer.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SurveyService} from "../service/survey.service";
import {Survey} from "../model/survey";
import {Subject, takeUntil} from "rxjs";
import {AnswerOption} from "../model/answer-option";
import {QuestionType} from "../model/question-type";
import {FormBuilder, FormGroup} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {UserAnswer} from "../model/user-answer";
import {SemanticService} from "../service/semantic-service.service";
import {QuestionService} from "../service/question.service";

export type AnswerOptionForm = { optionId: number, isChecked: boolean }

export type UserAnswerForm = {
  answerDate?: Date;
  answerOptions?: AnswerOptionForm[],
  answerSelect?: string,
  answerText?: string
}

@Component({
  selector: 'app-answer-survey',
  templateUrl: './answer-survey.component.html',
  styleUrls: ['./answer-survey.component.css']
})
export class AnswerSurveyComponent implements OnDestroy, OnInit {
  protected readonly QuestionType = QuestionType;
  private ngUnsubscribe = new Subject<void>();

  surveyForm: FormGroup = this.fb.group({});
  surveyId: number;
  survey: Survey;

  constructor(private fb: FormBuilder,
              private userAnswerService: UserAnswerService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar,
              private surveyService: SurveyService,
              private semanticService: SemanticService,
              private questionService: QuestionService,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe(params => {
      this.surveyId = params["id"];

      if (!this.surveyId) {
        this.router.navigate(['/']);
      }

      this.surveyService.get(this.surveyId).subscribe((survey: Survey) => {
        this.survey = survey;
        this.buildForm(survey);
      });
    });
  }

  buildForm(survey: Survey) {
    this.surveyForm = this.fb.group({
      questions: this.fb.array(survey.questions.map(question => this.fb.group({
        userAnswers: this.buildUserAnswer(question.answerOptions),
        questionId: question.id,
      })))
    });
  }

  buildUserAnswer(answerOptions: AnswerOption[]): FormGroup {
    return this.fb.group({
      answerOptions: this.fb.array(answerOptions.map(answerOption => this.fb.group({
        optionId: answerOption.id,
        isChecked: this.fb.control(false),
      }))),
      answerText: [''],
      answerDate: [null],
      answerSelect: [''],
    })
  }

  saveAnswers() {
    const questions = this.surveyForm.value.questions;
    const userAnswers: UserAnswer[] = this.mapQuestionsToUserAnswers(questions);
    this.userAnswerService.create(this.surveyId, userAnswers).subscribe((userAnswers: UserAnswer[]) => {
      this.openSnackBar('Answers saved successfully', 'snackbar-success');
      this.router.navigate(['/']);
      this.updatePositivePercentage();
    });
  }

  updatePositivePercentage() {
    const textQuestions = this.survey.questions.filter(question =>
      question.type == QuestionType.LONG_TEXT || question.type == QuestionType.SHORT_TEXT);
    textQuestions.forEach(question => {
      this.userAnswerService.getAllByQuestionId(this.surveyId, question.id!).subscribe((userAnswers: UserAnswer[]) => {
        this.updatePositivePercentageForTextQuestions(userAnswers);
      });
    });
  }

  updatePositivePercentageForTextQuestions(userAnswers: UserAnswer[]) {
    const userAnswersTextType = userAnswers.filter(answer => answer?.answerText);
    const textAnswers = this.toQuestionId_UserAnswersMap(userAnswersTextType);
    this.updateEachQuestionPositivePercentage(textAnswers);
  }

  private toQuestionId_UserAnswersMap(userAnswersTextType: UserAnswer[]): { [questionId: number]: string[] } {
    return userAnswersTextType.reduce((acc, userAnswer) => {
      if (!acc[userAnswer.questionId]) {
        acc[userAnswer.questionId] = [];
      }
      acc[userAnswer.questionId].push(userAnswer.answerText!);
      return acc;
    }, {} as { [questionId: number]: string[] });
  }

  private updateEachQuestionPositivePercentage(textAnswers: { [questionId: number]: string[] }) {
    for (let questionId in textAnswers) {
      this.semanticService.getPositivePercentage(textAnswers[questionId]).subscribe((positivePercentage: number) => {
        this.questionService.patch(this.safeNumberConversion(questionId)!, {sentimentPrediction: positivePercentage})
          .subscribe();
      });
    }
  }

  mapQuestionsToUserAnswers(questions: any[]): UserAnswer[] {
    return questions.map((question: { questionId: number, userAnswers: UserAnswerForm }) => {
      const selectedAnswerOptionIds: number[] = question.userAnswers?.answerOptions
        ? question.userAnswers.answerOptions.filter((answerOption: AnswerOptionForm) => answerOption?.isChecked)
          .map((answerOption: AnswerOptionForm) => answerOption.optionId)
        : [];
      return {
        answerDate: question.userAnswers.answerDate,
        answerOptionId: this.safeNumberConversion(question.userAnswers.answerSelect),
        answerOptionIds: selectedAnswerOptionIds,
        answerText: question.userAnswers.answerText,
        questionId: question.questionId,
      };
    });
  }

  safeNumberConversion(value?: string): number | undefined {
    const num = Number(value);
    return isNaN(num) ? undefined : num;
  }

  sortAnswerOptions(answerOptions: AnswerOption[]): AnswerOption[] {
    return answerOptions.sort((a, b) => (a.order || 0) - (b.order || 0));
  }

  openSnackBar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: [panelClass]
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
