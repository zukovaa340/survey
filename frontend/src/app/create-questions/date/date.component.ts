import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Question } from "../../model/question";

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent {

  @Input() question!: Question;
  @Output() questionChangedEmitter = new EventEmitter<Question>();
  @Output() removeQuestionEmitter = new EventEmitter<number>();

  ngOnChanges() {
    this.questionChangedEmitter.emit(this.question);
  }

  removeQuestion() {
    this.removeQuestionEmitter.emit();
  }

  onInput(event: any) {
    const textarea = event.target;
    textarea.style.height = 'auto'; // Reset the height to auto
    textarea.style.height = `${textarea.scrollHeight}px`; // Set the height to the scrollHeight
  }

}
