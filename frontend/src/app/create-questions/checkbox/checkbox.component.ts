import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Question } from "../../model/question";

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {

  @Input() question!: Question;
  @Output() questionChangedEmitter = new EventEmitter<Question>();
  @Output() removeQuestionEmitter = new EventEmitter<number>();
  @Output() addOptionEmitter = new EventEmitter<void>();
  @Output() removeOptionEmitter = new EventEmitter<number>();

  ngOnChanges() {
    this.questionChangedEmitter.emit(this.question);
  }

  addOption() {
    this.addOptionEmitter.emit();
  }

  removeOption(index: number) {
    this.removeOptionEmitter.emit(index);
  }

  removeQuestion() {
    this.removeQuestionEmitter.emit();
  }


  onInput(event: any) {
    const textarea = event.target;
    textarea.style.height = 'auto'; // Reset the height to auto
    textarea.style.height = `${textarea.scrollHeight}px`; // Set the height to the scrollHeight
  }
}
