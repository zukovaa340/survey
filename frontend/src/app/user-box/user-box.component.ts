import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { MatSnackBar } from "@angular/material/snack-bar";
import { User } from "../model/user";
import { MatDialog } from "@angular/material/dialog";
import { SignInComponent } from "../sign-in/sign-in.component";
import { SignUpComponent } from "../sign-up/sign-up.component";

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
  styleUrls: ['./user-box.component.css']
})
export class UserBoxComponent implements OnInit {
  user?: User;
  userNameToDisplay: string;

  constructor(private authService: AuthService, private snackBar: MatSnackBar, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.user = this.authService.user;
    this.userNameToDisplay = this.getUserNameToDisplay();

  }

  openSignInDialog(): void {
    const dialogRef = this.dialog.open(SignInComponent);

    dialogRef.afterClosed().subscribe((res: { user?: User, openSignUp?: boolean }) => {
      this.user = res?.user ?? this.authService.user;
      this.userNameToDisplay = this.getUserNameToDisplay();

      if (res?.openSignUp) this.openSignUpDialog();
    });
  }

  openSignUpDialog(): void {
    const dialogRef = this.dialog.open(SignUpComponent);

    dialogRef.afterClosed().subscribe((res: { user?: User, openSignIn?: boolean }) => {
      this.user = res?.user ?? this.authService.user;
      this.userNameToDisplay = this.getUserNameToDisplay();

      if (res?.openSignIn) this.openSignInDialog();
    });
  }

  signOut(): void {
    this.authService.signOut().subscribe(() => {
      this.user = this.authService.user;
      this.userNameToDisplay = this.getUserNameToDisplay();

      this.snackBar.open('You are successfuly signed out', 'Close', {
        duration: 3000,
        panelClass: ['snackbar-success']
      });
    });
  }

  getUserNameToDisplay(): string {
    console.log('isAuthenticated', this.authService.isAuthenticated())
    if (!this.authService.isAuthenticated()) {
      return 'Guest';
    }

    return (this.user?.firstName && this.user.lastName)
      ? (this.user.firstName + ' ' + this.user.lastName)
      : this.user?.email ?? 'Guest';
  }

}
