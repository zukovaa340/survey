import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { ViewSurveysComponent } from './view-surveys/view-surveys.component';
import { MatListModule } from '@angular/material/list';
import { CreateSurveysComponent } from './create-surveys/create-surveys.component';
import { AnalysingSurveysComponent } from './analysing-surveys/analysing-surveys.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CheckboxComponent } from './create-questions/checkbox/checkbox.component';
import { InputComponent } from './create-questions/input/input.component';
import { TextareaComponent } from './create-questions/textarea/textarea.component';
import { SelectComponent } from './create-questions/select/select.component';
import { DateComponent } from './create-questions/date/date.component';
import { AnswerSurveyComponent } from './answer-survey/answer-survey.component';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { EditSurveysComponent } from './edit-surveys/edit-surveys.component';
import { NgChartsModule } from 'ng2-charts';
import { AnswersAnalysingComponent } from './answers-analysing/answers-analysing.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { AuthInterceptor } from './interceptor/auth-interceptor.service';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { InputSearchComponent } from './input-search/input-search.component';
import { UserBoxComponent } from './user-box/user-box.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MessageComponent } from './message/message.component';
import { GlobalErrorHandler } from "./interceptor/global-error-handler";
import { MatTableModule } from "@angular/material/table";
import {QuestionBarChartComponent} from "./answers-analysing/question-bar-chart/question-bar-chart.component";
import {QuestionDateChartComponent} from "./answers-analysing/question-date-chart/question-date-chart.component";
import {
    TextInputAnalyseCardComponent
} from "./answers-analysing/text-input-analyse-card/text-input-analyse-card.component";
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    ViewSurveysComponent,
    CreateSurveysComponent,
    AnalysingSurveysComponent,
    CheckboxComponent,
    InputComponent,
    TextareaComponent,
    SelectComponent,
    DateComponent,
    AnswerSurveyComponent,
    EditSurveysComponent,
    AnswersAnalysingComponent,
    SignInComponent,
    SignUpComponent,
    UserBoxComponent,
    MessageComponent,
  ],
    imports: [
        MatTableModule,
        NgChartsModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatCardModule,
        MatButtonModule,
        AppRoutingModule,
        MatListModule,
        LayoutModule,
        MatIconModule,
        MatFormFieldModule,
        FormsModule,
        MatMenuModule,
        MatInputModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        HttpClientModule,
        MatDialogModule,
        MatSnackBarModule,
        QuestionBarChartComponent,
        QuestionDateChartComponent,
        TextInputAnalyseCardComponent,
        InputSearchComponent,
        MatTooltipModule,
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {provide: ErrorHandler, useClass: GlobalErrorHandler}
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
