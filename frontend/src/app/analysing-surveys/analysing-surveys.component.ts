import {Component, OnInit} from '@angular/core';
import {ChartData, ChartType} from 'chart.js';
import {Page} from "../model/page";
import {Survey} from "../model/survey";
import {SurveyService} from "../service/survey.service";
import {MatDialog} from "@angular/material/dialog";
import {PageEvent} from "@angular/material/paginator";
import {Question} from "../model/question";

@Component({
  selector: 'app-analysing-surveys',
  templateUrl: './analysing-surveys.component.html',
  styleUrls: ['./analysing-surveys.component.css']
})
export class AnalysingSurveysComponent implements OnInit {
  public searchKey?: string;
  public surveys?: Page<Survey[]>;
  public pageSize: number = 5;
  public doughnutChartType: ChartType = 'doughnut';
  public maxQuestionsInSurveys: number;

  constructor(public surveyService: SurveyService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.initPageWithoutParams(surveys => this.surveys = surveys);
    this.surveyService.getMaxQuestionsInSurveys().subscribe(maxQuestions => this.maxQuestionsInSurveys = maxQuestions);
  }

  initPageWithoutParams(callback: (surveys: Page<Survey[]>) => void): void {
    this.surveyService.getAll().subscribe(callback);
  }

  initPage($event: PageEvent): void {
    this.surveyService.getAll($event.pageIndex, this.pageSize, this.searchKey).subscribe(surveys => {
      this.surveys = surveys;
    });
  }

  predictCompletionTime(questions: Question[]): number {
    let totalTime = 0;

    for (let question of questions) {
      switch (question.type) {
        case 'SINGLE_CHOICE':
          totalTime += 0.6;
          break;
        case 'MULTIPLE_CHOICE':
          totalTime += 1;
          break;
        case 'SHORT_TEXT':
          totalTime += 1.8;
          break;
        case 'LONG_TEXT':
          totalTime += 2.7;
          break;
        case 'DATE':
          totalTime += 0.8;
          break;
      }
    }

    return totalTime;
  }

  doughnutChart(questions: Question[]): any {
    const maxTimePerQuestion = 2.7;
    const maximumTimeOfCompletion: number = Math.round(this.maxQuestionsInSurveys * maxTimePerQuestion);
    const predictedTimeOfCompletion: number = Math.round(this.predictCompletionTime(questions));
    const doughnutChartData: ChartData<'doughnut'> = {
      datasets: [
        {
          data: [predictedTimeOfCompletion, maximumTimeOfCompletion],
          backgroundColor: ['#3B3E91', '#C5C5E7']
        }
      ]
    };
    return doughnutChartData;
  }

  protected readonly Math = Math;
}
