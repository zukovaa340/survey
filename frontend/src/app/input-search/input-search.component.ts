import {Component, EventEmitter, Output} from '@angular/core';
import {CommonModule} from "@angular/common";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
  ],
})
export class InputSearchComponent {
  @Output() onSearch: EventEmitter<string> = new EventEmitter();
  @Output() onReset: EventEmitter<string> = new EventEmitter();

  searchTitle(title: string): void {
    this.onSearch.emit(title);
  }

  resetSearch(input: HTMLInputElement): void {
    this.onReset.emit();
    input.value = '';
    input.focus();
  }
}
