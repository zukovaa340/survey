import {Component} from '@angular/core';
import {Question} from "../model/question";
import {SurveyService} from "../service/survey.service";
import {Router} from "@angular/router";
import {QuestionType} from "../model/question-type";
import {FormControl, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";


@Component({
  selector: 'app-create-surveys',
  templateUrl: './create-surveys.component.html',
  styleUrls: ['./create-surveys.component.css']
})


export class CreateSurveysComponent {
  surveyTitle = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(200)]);
  public questions: Question[] = [];
  public QuestionType = QuestionType;

  constructor(private router: Router,
              private surveyService: SurveyService,
              private snackBar: MatSnackBar) {
  }

  addCheckboxQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.MULTIPLE_CHOICE);
    this.questions = [...this.questions, newQuestion];
  }

  addSelectQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.SINGLE_CHOICE);
    this.questions = [...this.questions, newQuestion];
  }

  addInputQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.SHORT_TEXT);
    this.questions = [...this.questions, newQuestion];
  }

  addTextAreaQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.LONG_TEXT);
    this.questions = [...this.questions, newQuestion];
  }

  addDateQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.DATE);
    this.questions = [...this.questions, newQuestion];
  }

  removeQuestion(index: number) {
    this.questions.splice(index, 1);
  }

  saveSurvey() {
    this.surveyService.create({title: this.surveyTitle.value || '', questions: this.questions}).subscribe(
      () => {
        this.router.navigate(['']).then(r => {
          this.openSnackBar('Updated successfully', 'snackbar-success');
        });
      }
    );
  }

  openSnackBar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: [panelClass]
    });
  }

  onRemoveOption(i: number, $event: number) {
    if (this?.questions[i] && this.questions[i]?.answerOptions) {
      this.questions[i].answerOptions.splice($event, 1);
      console.log('this.questions=', this.questions)
    }
  }

  onAddOption(i: number) {
    if (this.questions[i] && this.questions[i].answerOptions) {
      const lastAnswerOption = this.questions[i].answerOptions[this.questions[i].answerOptions.length - 1];
      const order = lastAnswerOption?.order ? lastAnswerOption.order + 1 : 0;
      this.questions[i].answerOptions.push({order: order, title: ''});
    } else {
      this.questions[i].answerOptions.push({order: 0, title: ''});
    }
  }

  onRemoveQuestion(i: number) {
    this.questions.splice(i, 1);
  }

  onQuestionChanged(question: Question) {
    const index = this.questions.indexOf(question);
    if (index !== -1) {
      this.questions[index] = question;
    }
  }


  getTitleErrorMessage() {
    if (this.surveyTitle.hasError('required')) {
      return 'You must enter a value';
    }

    return this.surveyTitle.hasError('minlength') ? 'Minimum 5 symbols' :
      this.surveyTitle.hasError('maxlength') ? 'Maximum 200 symbols' : '';
  }

}
