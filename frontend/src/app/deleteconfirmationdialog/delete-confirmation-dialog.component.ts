import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";

@Component({
  selector: 'app-deleteconfirmationdialog',
  standalone: true,
  templateUrl: './delete-confirmation-dialog.component.html',
  imports: [
    MatDialogModule,
    MatButtonModule
  ],
  styleUrls: ['./delete-confirmation-dialog.component.css']
})
export class DeleteConfirmationDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: {name: string}) { }
}
