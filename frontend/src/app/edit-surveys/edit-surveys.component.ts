import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SurveyService} from "../service/survey.service";
import {Question} from "../model/question";
import {Survey} from "../model/survey";
import {QuestionType} from "../model/question-type";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-edit-surveys',
  templateUrl: './edit-surveys.component.html',
  styleUrls: ['./edit-surveys.component.css']
})
export class EditSurveysComponent implements OnInit {
  public survey!: Survey;
  public questions: Question[] = [];
  public QuestionType = QuestionType;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private surveyService: SurveyService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id: number = +params['id'];
      this.surveyService.get(id).subscribe(survey => {
        this.survey = survey;
        this.questions = survey.questions;
      });
    });
  }

  addCheckboxQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.MULTIPLE_CHOICE);
    this.questions = [...this.questions, newQuestion];
  }

  addSelectQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.SINGLE_CHOICE);
    this.questions = [...this.questions, newQuestion];
  }

  addInputQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.SHORT_TEXT);
    this.questions = [...this.questions, newQuestion];
  }

  addTextAreaQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.LONG_TEXT);
    this.questions = [...this.questions, newQuestion];
  }

  addDateQuestion() {
    const newQuestion: Question = new Question(
      [], [], undefined,
      '', QuestionType.DATE);
    this.questions = [...this.questions, newQuestion];
  }

  removeQuestion(index: number) {
    this.questions.splice(index, 1);
  }

  saveSurvey() {
    this.survey.questions = this.questions;
    if (this.survey.id) {
      this.surveyService.update(this.survey.id, this.survey).subscribe(() => {
        this.router.navigate(['']).then(() => {
          this.openSnackBar('Updated successfully', 'snackbar-success');
        });
      });
    }

  }

  openSnackBar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: [panelClass]
    });
  }

  onRemoveOption(i: number, $event: number) {
    if (this?.questions[i] && this.questions[i]?.answerOptions) {
      this.questions[i].answerOptions.splice($event, 1);
      console.log('this.questions=', this.questions)
    }
  }

  onAddOption(i: number) {
    if (this.questions[i] && this.questions[i].answerOptions) {
      const lastAnswerOption = this.questions[i].answerOptions[this.questions[i].answerOptions.length - 1];
      const order = lastAnswerOption?.order ? lastAnswerOption.order + 1 : 0;
      this.questions[i].answerOptions.push({order: order, title: ''});
    } else {
      this.questions[i].answerOptions.push({order: 0, title: ''});
    }
  }

  onRemoveQuestion(i: number) {
    this.questions.splice(i, 1);
  }

  onQuestionChanged(question: Question) {
    const index = this.questions.indexOf(question);
    if (index !== -1) {
      this.questions[index] = question;
    }
  }
}
