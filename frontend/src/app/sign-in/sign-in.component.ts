import { Component, OnInit, Optional } from '@angular/core';
import { AuthService } from "../service/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { User } from "../model/user";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  signInForm: FormGroup;

  constructor(private authService: AuthService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              @Optional() public dialogRef: MatDialogRef<SignInComponent>) {
  }

  ngOnInit() {
    this.signInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(16), Validators.pattern(/^[a-zA-Z0-9]*$/)]]
    });
  }

  signIn() {
    if (this.signInForm.valid) {
      const { email, password } = this.signInForm.value;
      this.authService.signIn(email, password).subscribe({
        next: (user: User) => {
          this.authService.updateUserLocalStorage(user);
          this.openSnackBar('Congratulations! Sign in successful. ', 'snackbar-success');
          this.closeDialog(user);
        },
        error: () => this.openSnackBar('Sign in failed. Check inputs and try again', 'snackbar-error')
      });
    }
  }

  openSnackBar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: [panelClass]
    });
  }

  openSignUpDialog(): void {
    this.dialogRef.close({ user: undefined, openSignUp: true });
  }

  closeDialog(user?: User): void {
    this.dialogRef.close(user);
  }

}
