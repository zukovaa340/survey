import { Component, OnInit, Optional } from '@angular/core';
import { UserService } from "../service/user.service";
import { User } from "../model/user";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatDialogRef } from "@angular/material/dialog";
import CustomValidators from "../validator/custom-validators";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              @Optional() public dialogRef: MatDialogRef<SignUpComponent>) {
  }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', [Validators.pattern(/^[0-9]*$/)]],
        password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(16), Validators.pattern(/^[a-zA-Z0-9]*$/)]],
        confirmPassword: ['']
      },
      {
        validators: [CustomValidators.match('password', 'confirmPassword')]
      }
    );
  }

  signUp() {
    if (this.signUpForm.valid) {
      this.userService.signUp(this.signUpForm.value).subscribe({
        next: (user: User) => {
          this.dialogRef.close({ user: user, openSignUp: false, openSignIn: false });
          this.openSnackBar('Registration successful ', 'snackbar-success');
        },
        error: () => this.openSnackBar('Registration failed ', 'snackbar-error')
      });
    }
  }

  openSnackBar(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass: [panelClass]
    });
  }

  openSignInDialog(): void {
    this.dialogRef.close({ user: undefined, openSignUp: false, openSignIn: true });
  }

}
