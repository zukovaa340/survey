package com.example.survey;

import com.example.survey.model.Role;
import com.example.survey.model.Status;
import com.example.survey.model.User;
import com.example.survey.repository.UserRepository;
import com.example.survey.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    
    @Mock
    private UserRepository userRepository;
    
    @Mock
    private PasswordEncoder passwordEncoder;
    
    @InjectMocks
    private UserService userService;
    
    private User user;
    
    @BeforeEach
    public void setUp() {
        user = User.builder()
                .id(1L)
                .phone("12312312")
                .role(Role.USER)
                .status(Status.ACTIVE)
                .email("text@example.com")
                .firstName("firstname")
                .lastName("lastname")
                .password("password")
                .build();
    }
    
    @Test
    void testAddUser() {
        when(passwordEncoder.encode(user.getPassword())).thenReturn("encodedPassword");
        when(userRepository.save(user)).thenReturn(user);
        
        User result = userService.addUser(user);
        
        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(user);
        assertEquals(user, result);
    }
    
    @Test
    void testCreatePlainUser() {
        when(passwordEncoder.encode(user.getPassword())).thenReturn("encodedPassword");
        when(userRepository.save(user)).thenReturn(user);
        
        User result = userService.createPlainUser(user, "password");
        
        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(user);
        assertEquals(user, result);
    }
    
    @Test
    void testUpdateUser() {
        User updatedUser = new User();
        updatedUser.setId(1L);
        updatedUser.setEmail("updated@example.com");
        updatedUser.setPassword("updatedPassword");
        
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(updatedUser);
        
        User result = userService.updateUser(user.getId(), updatedUser);
        
        verify(userRepository, times(1)).findById(user.getId());
        verify(userRepository, times(1)).save(user);
        assertEquals(updatedUser, result);
    }
    
    @Test
    void testGetById() {
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        
        User result = userService.getById(user.getId());
        
        verify(userRepository, times(1)).findById(user.getId());
        assertEquals(user, result);
    }
    
    @Test
    void testDeleteUserById() {
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        doNothing().when(userRepository).deleteById(user.getId());
        
        userService.deleteUserById(user.getId());
        
        verify(userRepository, times(1)).findById(user.getId());
        verify(userRepository, times(1)).deleteById(user.getId());
    }
}