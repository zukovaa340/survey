package com.example.survey;

import com.example.survey.model.Role;
import com.example.survey.model.Status;
import com.example.survey.model.User;
import com.example.survey.model.dto.UserRequest;
import com.example.survey.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private User user;
    
    
    @BeforeEach
    public void setUp() {
        user = User.builder()
                .id(1L)
                .phone("12312312")
                .role(Role.ADMIN)
                .status(Status.ACTIVE)
                .email("text@example.com")
                .firstName("firstname")
                .lastName("lastname")
                .password("password")
                .build();
    }
    
/*    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetAllUsers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetUserById() throws Exception {
        User user = new User();
        user.setFirstName("Test User");
        User createdUser = userService.addUser(user);
        
        mockMvc.perform(MockMvcRequestBuilders.get("/users/" + createdUser.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", Matchers.is("Test User")));
    }
    
    @Test
    @WithMockUser(roles = "ADMIN")
    void testGetUserByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/999999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
    
    @Test
    @WithMockUser(roles = "ADMIN")
    void testCreateUser() throws Exception {
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("zzzzzzz@gmail.com");
        userRequest.setRole(Role.ADMIN);
        userRequest.setPassword("qweqweqw");
        userRequest.setStatus(Status.ACTIVE);
        
        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(userRequest)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.is("zzzzzzz@gmail.com")));
    }
    
    @Test
    void testForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(user)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }*/
    
}