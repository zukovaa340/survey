package com.example.survey.mapper;

import com.example.survey.model.AnswerOption;
import com.example.survey.model.Question;
import com.example.survey.model.UserAnswer;
import com.example.survey.model.dto.UserAnswerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {AnswerOptionMapper.class})
public interface UserAnswerMapper {
    
    @Mapping(source = "question", target = "questionId", qualifiedByName = "mapQuestionToId")
    @Mapping(source = "answerOptions", target = "answerOptionIds", qualifiedByName = "mapToIds")
    UserAnswerDto toDto(UserAnswer userAnswer);
    
    @Mapping(source = "questionId", target = "question", qualifiedByName = "mapIdToQuestion")
    @Mapping(source = "answerOptionIds", target = "answerOptions", qualifiedByName = "mapToAnswerOptions")
    UserAnswer toEntity(UserAnswerDto userAnswerDto);
    
    List<UserAnswer> toEntityList(List<UserAnswerDto> userAnswerDtos);
    
    List<UserAnswerDto> toDtoList(List<UserAnswer> userAnswers);
    
    @Named("mapToIds")
    default Set<Integer> mapToIds(Set<AnswerOption> value) {
        if (value == null) return Collections.EMPTY_SET;
        return value.stream()
                .map(AnswerOption::getId)
                .collect(Collectors.toSet());
    }
    
    @Named("mapToAnswerOptions")
    default Set<AnswerOption> mapToAnswerOptions(Set<Integer> value) {
        if (value == null) return Collections.EMPTY_SET;
        return value.stream()
                .map(id -> AnswerOption.builder().id(id).build())
                .collect(Collectors.toSet());
    }
    
    @Named("mapIdToQuestion")
    default Question mapToAnswerOptions(Integer questionId) {
        return Question.builder().id(questionId).build();
    }
    
    @Named("mapQuestionToId")
    default Integer mapQuestionToId(Question question) {
        return question.getId();
    }
    
}
