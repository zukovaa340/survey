package com.example.survey.mapper;

import com.example.survey.model.User;
import com.example.survey.model.dto.AuthenticatedUserResponse;
import com.example.survey.model.dto.UserRequest;
import com.example.survey.model.dto.UserResponse;
import com.example.survey.model.dto.UserSignUpRequest;
import com.example.survey.security.jwt.AuthenticatedUser;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    
    UserResponse toUserResponse(User user);
    
    User toEntity(UserRequest userRequest);
    
    User toEntity(UserSignUpRequest userRequest);
    
    UserRequest toRequest(User user);
    
    AuthenticatedUserResponse toAuthenticatedUserResponse(AuthenticatedUser authenticatedUser);
}
