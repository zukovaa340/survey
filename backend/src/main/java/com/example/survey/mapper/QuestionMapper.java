package com.example.survey.mapper;

import com.example.survey.model.Question;
import com.example.survey.model.UserAnswer;
import com.example.survey.model.dto.QuestionDto;
import com.example.survey.model.dto.UserAnswerDto;
import com.example.survey.service.UserAnswerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AnswerOptionMapper.class})
public abstract class QuestionMapper {

    @Autowired
    private UserAnswerService userAnswerService;

    @Autowired
    private UserAnswerMapper userAnswerMapper;

    //question.id is the source it's normal that IDE shows cant recognize it
    @Mapping(source = "question.id", target = "userAnswers", qualifiedByName = "mapUserAnswers", conditionExpression = "java(fetchAnswers == true)")
    public abstract QuestionDto toDto(Question question, boolean fetchAnswers);

    public abstract Question toEntity(QuestionDto questionDto);

    public abstract List<QuestionDto> toDtoList(List<Question> questions);

    @Named("mapUserAnswers")
    protected List<UserAnswerDto> mapUserAnswers(Integer questionId) {
        var userAnswers = userAnswerService.getAllByQuestionId(questionId);
        return userAnswerMapper.toDtoList(userAnswers);
    }
}
