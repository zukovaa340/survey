package com.example.survey.mapper;

import com.example.survey.model.Question;
import com.example.survey.model.Survey;
import com.example.survey.model.dto.QuestionDto;
import com.example.survey.model.dto.SurveyDto;
import com.example.survey.service.UserAnswerService;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AnswerOptionMapper.class, UserAnswerMapper.class, QuestionMapper.class})
public abstract class SurveyMapper {

    @Autowired
    private UserAnswerService userAnswerService;

    @Autowired
    private UserAnswerMapper userAnswerMapper;

    @Autowired
    private QuestionMapper questionMapper;

    @Named("toDto")
    @Mapping(source = "author.id", target = "authorId")
    public abstract SurveyDto toDto(Survey survey);

    @Named("toDtoWithAnswers")
    @Mapping(source = "author.id", target = "authorId")
    @Mapping(target = "questions", qualifiedByName = "mapQuestionsWithAnswers") // source = "questions" is implicit
    public abstract SurveyDto toDtoWithAnswers(Survey survey);

    public abstract Survey toEntity(SurveyDto surveyDto);

    @IterableMapping(qualifiedByName = "toDto")
    public abstract List<SurveyDto> toDtos(List<Survey> survey);

    @Named("mapQuestionsWithAnswers")
    List<QuestionDto> mapQuestionsWithAnswers(List<Question> questions) {
        return questions.stream()
                .map(q -> questionMapper.toDto(q, true))
                .toList();
    }

}
