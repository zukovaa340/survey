package com.example.survey.mapper;

import com.example.survey.model.AnswerOption;
import com.example.survey.model.dto.AnswerOptionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AnswerOption.class})
public interface AnswerOptionMapper {
    
    AnswerOptionDto toDto(AnswerOption answerOption);
    
    @Mapping(target = "userAnswers", ignore = true)
    AnswerOption toEntity(AnswerOptionDto answerOptionDto);
    
    List<AnswerOptionDto> toDtoList(List<AnswerOption> answerOptions);
}
