package com.example.survey.controller;

import com.example.survey.mapper.SurveyMapper;
import com.example.survey.model.dto.PageResponse;
import com.example.survey.model.dto.SurveyDto;
import com.example.survey.service.SurveyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import static org.springframework.data.domain.Sort.Direction.DESC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/surveys")
@RequiredArgsConstructor
public class SurveyController {
    private final SurveyMapper surveyMapper;
    private final SurveyService surveyService;

    @GetMapping
    public ResponseEntity<PageResponse<SurveyDto>> getAll(
            @RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "page", defaultValue = "0") Integer page,
            @RequestParam(name = "size", defaultValue = "5") Integer size) {
        var surveyPage = surveyService.findAll(title, PageRequest.of(page, size, Sort.by(DESC, "id")));
        var surveyDtos = surveyMapper.toDtos(surveyPage.getContent());
        var response = PageResponse.<SurveyDto>builder()
                .content(surveyDtos)
                .currentPage(surveyPage.getNumber())
                .totalItems(surveyPage.getTotalElements())
                .totalPages(surveyPage.getTotalPages())
                .build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/max-questions")
    public ResponseEntity<Integer> getMaxQuestionsInSurveys() {
        return new ResponseEntity<>(surveyService.findMaxQuestionsInSurveys(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SurveyDto> saveSurvey(@RequestBody SurveyDto surveyDto) {
        var surveyRequest = surveyMapper.toEntity(surveyDto);
        var savedSurvey = surveyService.save(surveyRequest);
        var surveyResponse = surveyMapper.toDto(savedSurvey);

        return new ResponseEntity<>(surveyResponse, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SurveyDto> updateSurvey(@RequestBody SurveyDto surveyDto) {
        var mappedEntity = surveyMapper.toEntity(surveyDto);
        var savedEntity = surveyService.update(mappedEntity);
        var surveyResponse = surveyMapper.toDto(savedEntity);

        return new ResponseEntity<>(surveyResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSurvey(@PathVariable("id") Integer id) {
        surveyService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SurveyDto> getById(@PathVariable("id") Integer id,
                                             @RequestParam(name = "fetchAnswers", defaultValue = "false",
                                                     required = false) boolean fetchAnswers) {
        var surveyDto = fetchAnswers
                ? surveyMapper.toDtoWithAnswers(surveyService.findById(id))
                : surveyMapper.toDto(surveyService.findById(id));

        return new ResponseEntity<>(surveyDto, HttpStatus.OK);
    }


}
