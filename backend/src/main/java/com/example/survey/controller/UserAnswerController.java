package com.example.survey.controller;

import com.example.survey.mapper.UserAnswerMapper;
import com.example.survey.model.dto.UserAnswerDto;
import com.example.survey.service.UserAnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/surveys/{surveyId}/")
@RequiredArgsConstructor
public class UserAnswerController {
    private final UserAnswerService userAnswerService;
    private final UserAnswerMapper userAnswerMapper;
    
    @PostMapping("/questions/{questionId}/user-answers")
    public ResponseEntity<UserAnswerDto> saveUserAnswer(@PathVariable("surveyId") Integer surveyId,
                                                        @PathVariable("questionId") Integer questionId,
                                                        @RequestBody UserAnswerDto userAnswerDto) {
        var userAnswerToSave = userAnswerMapper.toEntity(userAnswerDto);
        var savedUserAnswer = userAnswerService.save(questionId, userAnswerToSave);
        
        return new ResponseEntity<>(userAnswerMapper.toDto(savedUserAnswer), HttpStatus.OK);
    }
    
    @GetMapping("/user-answers")
    public ResponseEntity<List<UserAnswerDto>> getAnswers(@PathVariable("surveyId") Integer surveyId) {
        var answers = userAnswerService.getAllBySurveyId(surveyId);
        
        return new ResponseEntity<>(userAnswerMapper.toDtoList(answers), HttpStatus.OK);
    }

    @GetMapping("/questions/{questionId}/user-answers")
    public ResponseEntity<List<UserAnswerDto>> getAnswersByQuestionId(@PathVariable("surveyId") Integer surveyId,
                                                          @PathVariable("questionId") Integer questionId) {
        var updatedUserAnswers = userAnswerService.getAllByQuestionId(questionId);

        return new ResponseEntity<>(userAnswerMapper.toDtoList(updatedUserAnswers), HttpStatus.OK);
    }
    
    @PostMapping("/user-answers")
    public ResponseEntity<List<UserAnswerDto>> saveUserAnswers(@PathVariable("surveyId") Integer surveyId,
                                                              @RequestBody List<UserAnswerDto> userAnswerDtos) {
        var userAnswerToSave = userAnswerMapper.toEntityList(userAnswerDtos);
        var savedUserAnswer = userAnswerService.save(userAnswerToSave);
        
        return new ResponseEntity<>(userAnswerMapper.toDtoList(savedUserAnswer), HttpStatus.CREATED);
    }
    
    @PutMapping("/questions/{questionId}/user-answers/{id}")
    public ResponseEntity<UserAnswerDto> updateUserAnswer(@PathVariable("surveyId") Integer surveyId,
                                                          @PathVariable("questionId") Integer questionId,
                                                          @RequestBody UserAnswerDto userAnswerDto) {
        var userAnswerToUpdate = userAnswerMapper.toEntity(userAnswerDto);
        var updatedUserAnswer = userAnswerService.update(surveyId, questionId, userAnswerToUpdate);
        
        return new ResponseEntity<>(userAnswerMapper.toDto(updatedUserAnswer), HttpStatus.OK);
    }
    
    @DeleteMapping("/questions/{questionId}/user-answers/{id}")
    public ResponseEntity<Void> deleteUserAnswer(@PathVariable("id") Integer id) {
        userAnswerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
