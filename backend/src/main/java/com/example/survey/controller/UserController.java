package com.example.survey.controller;

import com.example.survey.common.ApplicationConstants;
import com.example.survey.mapper.UserMapper;
import com.example.survey.model.dto.PageResponse;
import com.example.survey.model.dto.UserRequest;
import com.example.survey.model.dto.UserResponse;
import com.example.survey.model.dto.UserSignUpRequest;
import com.example.survey.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    
    @GetMapping
    @Operation(summary = "Get all users", responses = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = PageResponse.class)))})
    public ResponseEntity<PageResponse<UserResponse>> getAll(
            @Parameter(name = "page", description = "Page number", example = "0")
            @RequestParam(name = "page", defaultValue = "0") final Integer page,
            @Parameter(name = "size", description = "Number of items per page", example = "10")
            @RequestParam(name = "size", defaultValue = ApplicationConstants.Web.
                    DefaultPagingAttributes.PAGE_SIZE) final Integer size,
            @Parameter(name = "sortDirection", description = "Sort direction", example = "ASC")
            @RequestParam(name = "sortDirection", defaultValue = ApplicationConstants.Web.
                    DefaultPagingAttributes.SORT_DIRECTION) final String sortDirection,
            @Parameter(name = "sortField", description = "Sort field", example = "id")
            @RequestParam(name = "sortField", defaultValue = ApplicationConstants.Web.
                    DefaultPagingAttributes.SORT_FIELD) final String sortField) {
        
        final var pageRequest = PageRequest.of(page,
                size, Sort.Direction.fromString(sortDirection), sortField);
        
        final var pageUserResponse = userService.findAll(pageRequest).map(userMapper::toUserResponse);
        
        final var pageResponse = PageResponse
                .<UserResponse>builder()
                .content(pageUserResponse.getContent())
                .totalItems(pageUserResponse.getTotalElements())
                .build();
        
        return new ResponseEntity<>(pageResponse, HttpStatus.OK);
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get a user by ID", responses = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = UserResponse.class))),
            @ApiResponse(responseCode = "404", description = "User not found")})
    public ResponseEntity<UserResponse> getById(@Parameter(description = "ID of the user to be obtained", example = "1")
                                                @PathVariable("id") Long id) {
        var userResponse = userMapper.toUserResponse(userService.getById(id));
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }
    
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a user by ID", responses = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "User not found")})
    public ResponseEntity<Void> deleteById(@Parameter(description = "ID of the user to be deleted", example = "1")
                                           @PathVariable("id") Long id) {
        userService.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @PostMapping
    @Operation(summary = "Create a new user", responses = {
            @ApiResponse(responseCode = "201", description = "User created successfully",
                    content = @Content(schema = @Schema(implementation = UserResponse.class)))})
    public ResponseEntity<UserResponse> create(@Parameter(description = "User details to be created", required = true)
                                               @RequestBody UserRequest userRequest) {
        var userFromRequest = userMapper.toEntity(userRequest);
        var savedUser = userService.addUser(userFromRequest);
        var userResponse = userMapper.toUserResponse(savedUser);
        
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }
    
    @PostMapping("/signup")
    @Operation(summary = "Registration of a new user", responses = {
            @ApiResponse(responseCode = "201", description = "Registration successful",
                    content = @Content(schema = @Schema(implementation = UserResponse.class)))})
    public ResponseEntity<Void> signup(@Parameter(description = "User details to be created", required = true)
                                       @RequestBody UserSignUpRequest userRequest) {
        var userFromRequest = userMapper.toEntity(userRequest);
        userService.createPlainUser(userFromRequest, userRequest.getConfirmPassword());
        
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
    @PutMapping("/{id}")
    @Operation(summary = "Update a user by ID", responses = {
            @ApiResponse(responseCode = "200", description = "User updated successfully",
                    content = @Content(schema = @Schema(implementation = UserResponse.class))),
            @ApiResponse(responseCode = "404", description = "User not found")})
    public ResponseEntity<UserResponse> update(@Parameter(description = "ID of the user to be updated", example = "1")
                                               @PathVariable("id") Long id,
                                               @Parameter(description = "User details to be updated", required = true)
                                               @RequestBody UserRequest userRequest) {
        var userFromRequest = userMapper.toEntity(userRequest);
        var updatedUser = userService.updateUser(id, userFromRequest);
        var userResponse = userMapper.toUserResponse(updatedUser);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }
}
