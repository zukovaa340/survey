package com.example.survey.controller;

import com.example.survey.model.dto.AuthenticatedUserResponse;
import com.example.survey.model.dto.AuthenticationRequest;
import com.example.survey.service.AuthenticationService;
import jakarta.annotation.Nonnull;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthenticationController {
    final AuthenticationService authenticationService;
    
    @PostMapping("/login")
    public ResponseEntity<AuthenticatedUserResponse> authenticate(
            @Valid @Nonnull @RequestBody final AuthenticationRequest authRequest) {
        final var response = authenticationService.authenticate(authRequest);
        
        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, response.getCookieAccessToken().toString())
                .header(HttpHeaders.SET_COOKIE, response.getCookieRefreshToken().toString())
                .body(response.getAuthenticatedUser());
    }
    
    @PostMapping("/refresh")
    public ResponseEntity<AuthenticatedUserResponse> refreshToken(
            @CookieValue(value = "${spring.security.jwt.refresh-cookie-name}") String refreshToken) {
        final var response = authenticationService.tokenRefresh(refreshToken);
        
        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, response.getCookieRefreshToken().toString())
                .header(HttpHeaders.SET_COOKIE, response.getCookieAccessToken().toString())
                .body(response.getAuthenticatedUser());
    }
    
    @PostMapping("/logout")
    public ResponseEntity<Void> logout() {
        final var response = authenticationService.logout();
        
        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, response.getCookieAccessToken().toString())
                .header(HttpHeaders.SET_COOKIE, response.getCookieRefreshToken().toString())
                .build();
    }
}
