package com.example.survey.controller;

import com.example.survey.mapper.QuestionMapper;
import com.example.survey.model.Question;
import com.example.survey.model.dto.QuestionDto;
import com.example.survey.service.QuestionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/questions")
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionService questionService;
    private final QuestionMapper questionMapper;
    private final ObjectMapper objectMapper;

    @PatchMapping("/{id}")
    public ResponseEntity<QuestionDto> patchQuestion(@PathVariable("id") Integer id,
                                                     @RequestBody Map<String, Object> fields) {
        var question = questionService.findById(id);
        fields.forEach((key, value) -> {
            var field = ReflectionUtils.findField(Question.class, key);
            if (field == null) {
                throw new IllegalArgumentException("Field " + key + " not found");
            }
            field.setAccessible(true);
            var convertedValue = objectMapper.convertValue(value, field.getType());
            ReflectionUtils.setField(field, question, convertedValue);

        });
        var updatedQuestion = questionService.update(question);

        return ResponseEntity.ok(questionMapper.toDto(updatedQuestion, false));
    }

}
