package com.example.survey.exception.handler;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.example.survey.exception.ErrorMessage;
import com.example.survey.exception.ExceptionMessage;
import com.example.survey.exception.NotFoundException;
import com.example.survey.exception.UserAlreadyExistException;
import jakarta.annotation.Nonnull;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

/**
 * Exception handler for application.
 */
@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    private final ExceptionMessage exceptionMessage;
    
    /**
     * Handle AuthenticationExceptions.
     */
    @ExceptionHandler({AuthenticationException.class, JWTVerificationException.class})
    public ResponseEntity<ErrorMessage> handleAuthenticationException(
            @Nonnull final HttpServletRequest request,
            @Nonnull final Exception exception) {
        log.error("Exception was thrown due authentication:", exception);
        
        final var message = ErrorMessage.builder()
                .status(HttpStatus.UNAUTHORIZED.value())
                .date(new Date())
                .description(exceptionMessage.getUnauthorizedErrorMessage())
                .url(request.getRequestURL().toString())
                .build();
        
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }
    
    /**
     * Handle AccessDeniedException.
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorMessage> handleAccessDeniedException(
            @Nonnull final HttpServletRequest request,
            @Nonnull final AccessDeniedException exception) {
        log.error("Exception was thrown because access was denied:", exception);
        
        final var message = ErrorMessage.builder()
                .status(HttpStatus.FORBIDDEN.value())
                .date(new Date())
                .description(exceptionMessage.getAccessDeniedErrorMessage())
                .url(request.getRequestURL().toString())
                .build();
        
        return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
    }
    
    /**
     * Handle ConstraintViolationException.
     */
    @ExceptionHandler({ConstraintViolationException.class, UserAlreadyExistException.class})
    public ResponseEntity<ErrorMessage> handleResourceNotFoundException(
            @Nonnull final HttpServletRequest request,
            @Nonnull final Exception exception) {
        log.error("Exception was thrown because passed data was not valid:", exception);
        
        final var message = ErrorMessage.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .date(new Date())
                .description(exceptionMessage.getBadRequestErrorMessage())
                .url(request.getRequestURL().toString())
                .build();
        
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }
    
    /**
     * Handle Not found exception.
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorMessage> handleNotFoundException(
            @NonNull final HttpServletRequest request,
            @NonNull final NotFoundException exception) {
        log.error("Exception was thrown because resource was not found:", exception);
        final var message = ErrorMessage.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .date(new Date())
                .description(exceptionMessage.getNotFoundErrorMessage())
                .url(request.getRequestURL().toString())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }
    
    /**
     * Handle all other exceptions.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handleAllExceptions(
            @Nonnull final Exception exception,
            @Nonnull final HttpServletRequest request) {
        log.error("Exception was thrown due to an exception:", exception);
        
        final var responseStatus =
                exception.getClass().getAnnotation(ResponseStatus.class);
        final var status =
                responseStatus != null ? responseStatus.value() : HttpStatus.INTERNAL_SERVER_ERROR;
        final var message = ErrorMessage.builder()
                .status(status.value())
                .date(new Date())
                .description(exceptionMessage.getInternalServerErrorMessage())
                .url(request.getRequestURL().toString())
                .build();
        
        return new ResponseEntity<>(message, status);
    }
}