package com.example.survey.exception;

import java.io.Serial;

/**
 * Exception that is thrown when a user attempts to register, but
 * the provided user login is already in use.
 */
public class UserAlreadyExistException extends RuntimeException {
    
    @Serial
    private static final long serialVersionUID = 686921757946674249L;
    
    public UserAlreadyExistException(String message) {
        super(message);
    }
}