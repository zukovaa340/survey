package com.example.survey.exception;

import java.io.Serial;

public class NotFoundException extends RuntimeException {
    
    @Serial
    private static final long serialVersionUID = 9098930446915322824L;
    
    public NotFoundException(String msg) {
        super(msg);
    }
}
