package com.example.survey.security.jwt;

import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static com.example.survey.common.ApplicationConstants.Security.Routes.LOGOUT_PATH;

/**
 * Filter that handles all HTTP requests to application.
 * It's check if the request have an access token
 * TODO for now we have a state while saving the user in the context. We should refactor it to save the token instead.
 */
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    
    JwtService jwtService;
    UserDetailsService jwtUserService;
    
    @Override
    protected void doFilterInternal(
            @Nonnull final HttpServletRequest request,
            @Nonnull final HttpServletResponse response,
            @Nonnull final FilterChain filterChain
    ) throws ServletException, IOException {
        if (request.getRequestURI().equals(LOGOUT_PATH)) {
            filterChain.doFilter(request, response);
            return;
        }
        final var token = jwtService.getAccessTokenFromRequest(request);
        
        if (token != null) {
            final var userDetails = jwtUserService.
                    loadUserByUsername(jwtService.extractSubject(token));
            
            if (jwtService.validateAccessToken(token, userDetails.getUsername()) &&
                    SecurityContextHolder.getContext().getAuthentication() == null) {
                final var authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        filterChain.doFilter(request, response);
    }
    
}
