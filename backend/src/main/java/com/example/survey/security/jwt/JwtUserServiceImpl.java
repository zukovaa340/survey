package com.example.survey.security.jwt;

import com.example.survey.repository.UserRepository;
import jakarta.annotation.Nonnull;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class JwtUserServiceImpl implements UserDetailsService {
    UserRepository userRepository;
    
    @Override
    public UserDetails loadUserByUsername(@Nonnull final String username) {
        log.debug("Entering in loadUserByUsername Method...");
        
        final var user = userRepository.findByEmail(username).orElseThrow(() ->
                new UsernameNotFoundException("The user cannot be loaded by " +
                        "the given username. The username may be incorrect " +
                        "or requested user doesn't exist"));
        
        log.debug("User found by username: {}", username);
        
        return AuthenticatedUser.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .password(user.getPassword())
                .role(user.getRole())
                .status(user.getStatus())
                .build();
    }
}
