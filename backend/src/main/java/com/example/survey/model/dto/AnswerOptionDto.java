package com.example.survey.model.dto;

import jakarta.annotation.Nullable;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AnswerOptionDto {

    @Nullable
    Integer id;

    @Size(min = 5, max = 200, message
            = "Title must be between 5 and 200 characters")
    String title;

    Integer order;

}
