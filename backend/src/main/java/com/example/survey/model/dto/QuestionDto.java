package com.example.survey.model.dto;

import com.example.survey.model.QuestionType;
import jakarta.annotation.Nullable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class QuestionDto {
    
    @Nullable
    Integer id;
    
    @Size(min = 5, max = 200, message
            = "Title must be between 5 and 200 characters")
    String title;
    
    @Enumerated(EnumType.STRING)
    QuestionType type;

    @Nullable
    Double sentimentPrediction;
    
    @Nullable
    List<AnswerOptionDto> answerOptions;
    
    @Nullable
    List<UserAnswerDto> userAnswers;
}
