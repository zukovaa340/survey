package com.example.survey.model.dto;

import com.example.survey.model.Role;
import com.example.survey.model.Status;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode
@ToString
@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthenticatedUserResponse {
    
    Long id;
    
    String firstName;
    
    String email;
    
    String lastName;
    
    String phone;
    
    @Enumerated(EnumType.STRING)
    Status status;
    
    @Enumerated(EnumType.STRING)
    Role role;
    
}