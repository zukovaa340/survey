package com.example.survey.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class User {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
    
    @Column(name = "first_name")
    String firstName;
    
    @Column(name = "last_name")
    String lastName;
    
    @Email
    @Column(name = "email", nullable = false, unique = true)
    String email;
    
    @Column(name = "password", nullable = false)
    String password;
    
    @Column(name = "phone")
    String phone;
    
    @CreatedDate
    @Column(name = "created")
    LocalDateTime created;
    
    @LastModifiedDate
    @Column(name = "updated")
    LocalDateTime updated;
    
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    Role role;
    
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    Status status;
    
}