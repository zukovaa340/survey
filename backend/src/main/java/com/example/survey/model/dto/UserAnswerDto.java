package com.example.survey.model.dto;

import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserAnswerDto {
    
    @Nullable
    String answerText;
    
    @Nullable
    LocalDate answerDate;
    
    @Nullable
    Integer answerOptionId;
    
    @Nullable
    Set<Integer> answerOptionIds;
    
    @NotNull
    Integer questionId;
    
}
