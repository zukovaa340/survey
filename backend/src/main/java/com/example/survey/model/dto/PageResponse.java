package com.example.survey.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Schema(description = "Page response model")
public class PageResponse<T> {
    @Schema(description = "Content of the page")
    List<T> content;
    
    @Schema(description = "Current page number")
    Integer currentPage;
    
    @Schema(description = "Total number of items")
    Long totalItems;
    
    @Schema(description = "Total number of pages")
    Integer totalPages;
}
