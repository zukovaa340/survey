package com.example.survey.model.dto;

import com.example.survey.model.Role;
import com.example.survey.model.Status;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "User response model")
public class UserResponse {
    
    @Schema(description = "Unique identifier of the user")
    private Long id;
    
    @Schema(description = "First name of the user")
    private String firstName;
    
    @Schema(description = "Last name of the user")
    private String lastName;
    
    @Schema(description = "Email of the user")
    private String email;
    
    @Schema(description = "Phone number of the user")
    private String phone;
    
    @Schema(description = "Role of the user")
    private Role role;
    
    @Schema(description = "Status of the user")
    private Status status;
    
}