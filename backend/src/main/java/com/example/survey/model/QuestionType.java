package com.example.survey.model;

public enum QuestionType {
    SINGLE_CHOICE,
    MULTIPLE_CHOICE,
    SHORT_TEXT,
    LONG_TEXT,
    DATE
}
