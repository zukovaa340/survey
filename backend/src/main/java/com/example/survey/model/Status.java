package com.example.survey.model;

/**
 * Statuses for user status.
 */
public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}