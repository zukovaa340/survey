package com.example.survey.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static com.example.survey.common.ApplicationConstants.DataValidation.MAX_SIZE_OF_PASSWORD;
import static com.example.survey.common.ApplicationConstants.DataValidation.MIN_SIZE_OF_PASSWORD;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "User request model for sign up")
public class UserSignUpRequest {
    
    @Schema(description = "First name of the user")
    private String firstName;
    
    @Schema(description = "Last name of the user")
    private String lastName;
    
    @NotBlank
    @NotEmpty
    @Schema(description = "Email of the user")
    private String email;
    
    @Size(min = MIN_SIZE_OF_PASSWORD,
            max = MAX_SIZE_OF_PASSWORD)
    @NotBlank
    @NotEmpty
    @Schema(description = "Password of the user")
    private String password;
    
    @Size(min = MIN_SIZE_OF_PASSWORD,
            max = MAX_SIZE_OF_PASSWORD)
    @NotBlank
    @NotEmpty
    @Schema(description = "Confirm password of the user")
    private String confirmPassword;
    
    @Schema(description = "Phone number of the user")
    private String phone;
    
}