package com.example.survey.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "user_answer")
public class UserAnswer {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "answer_text", columnDefinition = "TEXT")
    String answerText;
    
    LocalDate answerDate;
    
    @Column(name = "answer_option_id")
    Integer answerOptionId;
    
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id")
    Question question;
    
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "answeroption_useranswer",
            joinColumns = @JoinColumn(name = "user_answer_id"),
            inverseJoinColumns = @JoinColumn(name = "answer_option_id"))
    @JsonManagedReference
    Set<AnswerOption> answerOptions = new HashSet<>();
    
    public void addAnswerOption(AnswerOption answerOption) {
        this.answerOptions.add(answerOption);
        answerOption.getUserAnswers().add(this);
    }
    
    public void removeAnswerOption(Integer aoId) {
        var answerOption = this.answerOptions.stream().filter(ao ->
                Objects.equals(ao.getId(), aoId)).findFirst().orElse(null);
        
        if (answerOption != null) {
            this.answerOptions.remove(answerOption);
            answerOption.getUserAnswers().remove(this);
        }
    }
    
}
