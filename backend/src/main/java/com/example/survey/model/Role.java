package com.example.survey.model;

/**
 * Roles that are used for the implementation of security
 * and can be used in the Role in the future.
 */
public enum Role {
    /**
     * Specific moderator role.
     */
    USER,
    /**
     * Privilege admin role.
     */
    ADMIN
}
