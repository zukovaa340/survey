package com.example.survey.model;

public enum SemanticPrediction {
    POSITIVE,
    NEGATIVE
}
