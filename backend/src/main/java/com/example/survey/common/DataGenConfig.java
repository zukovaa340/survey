package com.example.survey.common;

import com.example.survey.service.DataGenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
@DependsOn(value = "DataGenService")
public class DataGenConfig {
    
    DataGenConfig(@Autowired DataGenService dataGenService) {
        dataGenService.generateData();
    }
    
}
