package com.example.survey.common;

import lombok.experimental.UtilityClass;

/**
 * Contains various constants used in the application.
 */
@UtilityClass
public class ApplicationConstants {
    
    /**
     * Inner utility class for constants related to security part.
     */
    @UtilityClass
    public class Web {
        
        /**
         * Inner utility class for constants that should be
         */
        @UtilityClass
        public class CookiePaths {
            public static final String REFRESH_TOKEN = "/api/auth/refresh";
            public static final String ACCESS_TOKEN = "/api";
        }
        
        /**
         * Inner utility class for constants related to security part.
         */
        @UtilityClass
        public class DefaultPagingAttributes {
            public static final String PAGE_SIZE = "20";
            public static final String SORT_DIRECTION = "DESC";
            public static final String SORT_FIELD = "id";
        }
        
    }
    
    /**
     * Inner utility class for constants related to security part.
     */
    @UtilityClass
    public class Security {
        
        /**
         * Inner class containing paths that need special security settings.
         */
        @UtilityClass
        public class Routes {
            public static final String LOGIN_PATH = "/auth/login";
            public static final String SIGNUP_PATH = "/users/signup";
            public static final String TOKEN_REFRESH = "/auth/refresh";
            public static final String LOGOUT_PATH = "/auth/logout";
            public static final String SURVEYS_PATH = "/surveys";
            
        }
    }
    
    /**
     * Inner utility class for constants related to security jwt claims part.
     */
    @UtilityClass
    public class JwtClaimNames {
        public static final String USER_ROLE = "role";
    }
    
    @UtilityClass
    public class DataValidation {
        public static final int MIN_SIZE_OF_EMAIL = 5;
        public static final int MAX_SIZE_OF_EMAIL = 100;
        public static final int MIN_SIZE_OF_PASSWORD = 4;
        public static final int MAX_SIZE_OF_PASSWORD = 16;
    }
    
    @UtilityClass
    public class ErrorMassage {
        public static final String UNAUTHENTICATED_ERROR_MESSAGE = "The resource you're trying to reach requires authentication. Please authenticate and try again.";
        public static final String UNAUTHORIZED_ERROR_MESSAGE = "You do not have the necessary permissions to access this resource.";
    }
}
