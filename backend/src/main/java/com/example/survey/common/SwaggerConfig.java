package com.example.survey.common;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"swagger && !prod"})
public class SwaggerConfig {
    private static final String SCHEME_NAME = "Bearer Authentication";
    private static final String SCHEME = "Bearer";
    
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(getInfo())
                .addSecurityItem(new SecurityRequirement().addList(SCHEME_NAME))
                .components(new Components().addSecuritySchemes(SCHEME_NAME, getSecurityScheme()));
    }
    
    private Info getInfo() {
        return new Info()
                .title("Swagger API")
                .version("2.4.0")
                .contact(new Contact().name("Zhukova Anna"));
    }
    
    private SecurityScheme getSecurityScheme() {
        return new SecurityScheme()
                .name(SCHEME_NAME)
                .type(SecurityScheme.Type.HTTP)
                .scheme(SCHEME)
                .in(SecurityScheme.In.COOKIE)
                .bearerFormat("JWT");
    }
    
}