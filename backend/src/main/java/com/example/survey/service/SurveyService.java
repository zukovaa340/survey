package com.example.survey.service;

import static com.example.survey.common.ApplicationConstants.ErrorMassage.UNAUTHORIZED_ERROR_MESSAGE;
import com.example.survey.exception.NotFoundException;
import com.example.survey.model.Role;
import com.example.survey.model.Survey;
import com.example.survey.repository.SurveyRepository;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
public class SurveyService {
    private final AuthenticationServiceImpl authenticationService;
    private final SurveyRepository surveyRepository;
    private final UserService userService;
    
    public Page<Survey> findAll(@Nullable String title, @Nonnull Pageable pageable) {
        if (title == null) {
            return surveyRepository.findAll(pageable);
        }
        
        return surveyRepository.findAllByTitleContainingIgnoreCase(title, pageable);
    }

    public Integer findMaxQuestionsInSurveys() {
        return surveyRepository.findMaxQuestionsInSurveys();
    }
    
    public Survey findById(@Nonnull Integer id) {
        return surveyRepository.findById(id).orElseThrow(() -> new NotFoundException("Survey is not found"));
    }

    @Transactional
    public Survey save(@Nonnull Survey survey) {
        var currUserId = authenticationService.getAuthenticatedUser().getId();
        var authorRef = userService.getReferenceById(currUserId);
        
        survey.setAuthor(authorRef);
        
        return surveyRepository.save(survey);
    }
    
    public Survey update(@Nonnull Survey surveyToSave) {
        var originalSurvey = findById(surveyToSave.getId());
        
        surveyToSave.setAuthor(originalSurvey.getAuthor());
        validateAction(surveyToSave);
        
        return surveyRepository.save(surveyToSave);
    }
    
    public void deleteById(@Nonnull Integer id) {
        validateAction(findById(id));
        surveyRepository.deleteById(id);
    }
    
    public void validateAction(@Nonnull Survey survey) {
        var authenticatedUser = authenticationService.getAuthenticatedUser();
        
        if (authenticatedUser.getRole() != Role.ADMIN &&
                (survey.getAuthor() == null || !authenticatedUser.getId().equals(survey.getAuthor().getId()))) {
            throw new AccessDeniedException(UNAUTHORIZED_ERROR_MESSAGE);
        }
    }
    
}
