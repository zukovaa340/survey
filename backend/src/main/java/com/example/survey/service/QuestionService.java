package com.example.survey.service;

import com.example.survey.exception.NotFoundException;
import com.example.survey.model.Question;
import com.example.survey.repository.QuestionRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
public class QuestionService {
    private final QuestionRepository questionRepository;
    
    public Question findById(Integer id) {
        return questionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Question not found"));
    }
    
    public Question gerReferenceById(Integer id) {
        return questionRepository.getReferenceById(id);
    }
    
    public Question save(Question question) {
        return questionRepository.save(question);
    }
    
    public Question update(Question question) {
        if (!questionRepository.existsById(question.getId())) {
            throw new NotFoundException("Question not found");
        }
        return questionRepository.save(question);
    }
    
    public void deleteById(Integer id) {
        questionRepository.deleteById(id);
    }
    
}