package com.example.survey.service;

import com.example.survey.model.AnswerOption;
import com.example.survey.model.Question;
import com.example.survey.model.QuestionType;
import com.example.survey.model.Role;
import com.example.survey.model.Status;
import com.example.survey.model.Survey;
import com.example.survey.model.User;
import com.example.survey.repository.SurveyRepository;
import com.example.survey.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service("DataGenService")
public class DataGenService {
    private final UserRepository userRepository;
    private final SurveyRepository surveyRepository;
    private final PasswordEncoder passwordEncoder;
    
    public void generateData() {
        if (userRepository.count() > 0 && surveyRepository.count() > 0) {
            log.info("Test data already exists. Skipping test data generation.");
            return;
        }
        log.info("Generating data...");
        
        final User admin = User.builder()
                .email("admin@gmail.com")
                .firstName("Volodymyr")
                .lastName("Zelenskiy")
                .password(passwordEncoder.encode("admin"))
                .role(Role.ADMIN)
                .phone("0961232233")
                .status(Status.ACTIVE)
                .build();
        
        final User user = User.builder()
                .email("user@gmail.com")
                .firstName("Pieter")
                .lastName("Parker")
                .password(passwordEncoder.encode("user"))
                .role(Role.USER)
                .phone("0999977332")
                .status(Status.ACTIVE)
                .build();
        
        final User user1 = User.builder()
                .email("user1@gmail.com")
                .firstName("Monica")
                .lastName("Bellucci")
                .password(passwordEncoder.encode("user"))
                .role(Role.USER)
                .phone("0999980029")
                .status(Status.NOT_ACTIVE)
                .build();
        
        final User user2 = User.builder()
                .email("user2@gmail.com")
                .firstName("Marry")
                .lastName("Hill")
                .password(passwordEncoder.encode("user"))
                .role(Role.USER)
                .phone("0998858634")
                .status(Status.NOT_ACTIVE)
                .build();
        
        final User user3 = User.builder()
                .email("user3@gmail.com")
                .firstName("Eva")
                .lastName("Green")
                .password(passwordEncoder.encode("user"))
                .role(Role.USER)
                .phone("0998347565")
                .status(Status.DELETED)
                .build();
        
        //---------------------------------SURVEY PP---------------------------------
        
        final AnswerOption answerOptionPP11 = AnswerOption.builder().title("Food products").order(0).build();
        final AnswerOption answerOptionPP12 = AnswerOption.builder().title("Electronics and gadgets").order(1).build();
        final AnswerOption answerOptionPP13 = AnswerOption.builder().title("Clothing and accessories").order(2).build();
        
        final AnswerOption answerOptionPP21 = AnswerOption.builder().title("Brand").order(0).build();
        final AnswerOption answerOptionPP22 = AnswerOption.builder().title("Quality").order(1).build();
        final AnswerOption answerOptionPP23 = AnswerOption.builder().title("Price").order(2).build();
        final AnswerOption answerOptionPP24 = AnswerOption.builder().title("Recommendations from friends").order(3).build();
        
        final AnswerOption answerOptionPP31 = AnswerOption.builder().title("Exclusively online").order(0).build();
        final AnswerOption answerOptionPP32 = AnswerOption.builder().title("Mostly online").order(1).build();
        final AnswerOption answerOptionPP33 = AnswerOption.builder().title("Evenly between online and offline").order(2).build();
        final AnswerOption answerOptionPP34 = AnswerOption.builder().title("Mostly offline").order(3).build();
        final AnswerOption answerOptionPP35 = AnswerOption.builder().title("Exclusively offline").order(4).build();
        
        final Question questionPP1 = Question.builder()
                .title("What products do you most frequently purchase for yourself?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionPP11, answerOptionPP12, answerOptionPP13))
                .build();
        final Question questionPP2 = Question.builder()
                .title("How do you choose products for purchase?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionPP21, answerOptionPP22, answerOptionPP23, answerOptionPP24))
                .build();
        final Question questionPP3 = Question.builder()
                .title("How often do you make online purchases compared to offline stores?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionPP31, answerOptionPP32, answerOptionPP33, answerOptionPP34, answerOptionPP35))
                .build();
        final Question questionPP4 = Question.builder()
                .title("What is the most important aspect for you when choosing a product?")
                .type(QuestionType.LONG_TEXT)
                .build();
        
        final Survey surveyPP = Survey.builder().title("Product Preferences?").questions(List.of(questionPP1, questionPP2, questionPP3, questionPP4)).author(user2).build();
        
        //---------------------------------SURVEY CSN---------------------------------
        
        final AnswerOption answerOptionCSN11 = AnswerOption.builder().title("Medical services").order(0).build();
        final AnswerOption answerOptionCSN12 = AnswerOption.builder().title("Financial services").order(1).build();
        final AnswerOption answerOptionCSN13 = AnswerOption.builder().title("Educational services").order(2).build();
        final AnswerOption answerOptionCSN14 = AnswerOption.builder().title("Social services").order(3).build();
        final AnswerOption answerOptionCSN15 = AnswerOption.builder().title("Entertainment and leisure services").order(4).build();
        
        final AnswerOption answerOptionCSN21 = AnswerOption.builder().title("Restaurants/cafes").order(0).build();
        final AnswerOption answerOptionCSN22 = AnswerOption.builder().title("Hotels/guest houses").order(1).build();
        final AnswerOption answerOptionCSN23 = AnswerOption.builder().title("Transportation services (taxis, buses)").order(2).build();
        final AnswerOption answerOptionCSN24 = AnswerOption.builder().title("Medical facilities").order(3).build();
        
        final AnswerOption answerOptionCSN31 = AnswerOption.builder().title("Ratings on well-known apps").order(0).build();
        final AnswerOption answerOptionCSN32 = AnswerOption.builder().title("Online reviews").order(1).build();
        final AnswerOption answerOptionCSN33 = AnswerOption.builder().title("Recommendations from friends").order(2).build();
        
        final Question questionCSN1 = Question.builder()
                .title("Place of residence")
                .type(QuestionType.SHORT_TEXT)
                .build();
        final Question questionCSN2 = Question.builder()
                .title("What services do you most frequently need?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionCSN11, answerOptionCSN12, answerOptionCSN13, answerOptionCSN14, answerOptionCSN15))
                .build();
        final Question questionCSN3 = Question.builder()
                .title("Which industry is lagging behind in your locality?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionCSN21, answerOptionCSN22, answerOptionCSN23, answerOptionCSN24))
                .build();
        final Question questionCSN4 = Question.builder()
                .title("How do you check ratings and reviews before choosing services?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionCSN31, answerOptionCSN32, answerOptionCSN33))
                .build();
        final Question questionCSN5 = Question.builder()
                .title("What additional services would you like to see in your locality?")
                .type(QuestionType.LONG_TEXT)
                .build();
        
        final Survey surveyCSN = Survey.builder().title("Customer Service Needs?").questions(List.of(questionCSN1, questionCSN2, questionCSN3, questionCSN4, questionCSN5)).author(user1).build();
        
        //---------------------------------SURVEY AAM---------------------------------
        
        final AnswerOption answerOptionAAM11 = AnswerOption.builder().title("Every day").order(0).build();
        final AnswerOption answerOptionAAM12 = AnswerOption.builder().title("Several times a week").order(1).build();
        final AnswerOption answerOptionAAM13 = AnswerOption.builder().title("Rarely").order(2).build();
        final AnswerOption answerOptionAAM14 = AnswerOption.builder().title("Do not use at all").order(3).build();
        
        final AnswerOption answerOptionAAM21 = AnswerOption.builder().title("News websites").order(0).build();
        final AnswerOption answerOptionAAM22 = AnswerOption.builder().title("Television").order(1).build();
        final AnswerOption answerOptionAAM23 = AnswerOption.builder().title("Podcasts and audiobooks").order(2).build();
        final AnswerOption answerOptionAAM24 = AnswerOption.builder().title("Social media").order(3).build();
        
        final AnswerOption answerOptionAAM31 = AnswerOption.builder().title("Television advertising").order(0).build();
        final AnswerOption answerOptionAAM32 = AnswerOption.builder().title("Internet advertising").order(1).build();
        final AnswerOption answerOptionAAM33 = AnswerOption.builder().title("Outdoor advertising banners").order(2).build();
        final AnswerOption answerOptionAAM34 = AnswerOption.builder().title("Social media advertising").order(3).build();
        final AnswerOption answerOptionAAM35 = AnswerOption.builder().title("Newspapers").order(4).build();
        final AnswerOption answerOptionAAM36 = AnswerOption.builder().title("Radio").order(5).build();
        
        final Question questionAAM1 = Question.builder()
                .title("How often do you use social media?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionAAM11, answerOptionAAM12, answerOptionAAM13, answerOptionAAM14))
                .build();
        final Question questionAAM2 = Question.builder()
                .title("How do you choose informational sources?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionAAM21, answerOptionAAM22, answerOptionAAM23, answerOptionAAM24))
                .build();
        final Question questionAAM3 = Question.builder()
                .title("Which form of advertising is most effective for you?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionAAM31, answerOptionAAM32, answerOptionAAM33, answerOptionAAM34, answerOptionAAM35, answerOptionAAM36))
                .build();
        
        final Survey surveyAAM = Survey.builder().title("Advertising and Media?").questions(List.of(questionAAM1, questionAAM2, questionAAM3)).author(user).build();
        
        //---------------------------------SURVEY TH---------------------------------
        
        final AnswerOption answerOptionTH11 = AnswerOption.builder().title("Constantly").order(0).build();
        final AnswerOption answerOptionTH12 = AnswerOption.builder().title("Several times an hour").order(1).build();
        final AnswerOption answerOptionTH13 = AnswerOption.builder().title("Rarely").order(2).build();
        final AnswerOption answerOptionTH14 = AnswerOption.builder().title("Only for calls").order(3).build();
        
        final AnswerOption answerOptionTH21 = AnswerOption.builder().title("Social media").order(0).build();
        final AnswerOption answerOptionTH22 = AnswerOption.builder().title("Messengers").order(1).build();
        final AnswerOption answerOptionTH23 = AnswerOption.builder().title("Health and fitness").order(2).build();
        final AnswerOption answerOptionTH24 = AnswerOption.builder().title("Entertainment").order(3).build();
        
        final AnswerOption answerOptionTH31 = AnswerOption.builder().title("Use regularly").order(0).build();
        final AnswerOption answerOptionTH32 = AnswerOption.builder().title("Occasionally").order(1).build();
        final AnswerOption answerOptionTH33 = AnswerOption.builder().title("Do not use at all").order(2).build();
        
        final AnswerOption answerOptionTH41 = AnswerOption.builder().title("Every day").order(0).build();
        final AnswerOption answerOptionTH42 = AnswerOption.builder().title("Several times a week").order(1).build();
        final AnswerOption answerOptionTH43 = AnswerOption.builder().title("Rarely").order(2).build();
        final AnswerOption answerOptionTH44 = AnswerOption.builder().title("Do not use at all").order(3).build();
        
        final Question questionTH1 = Question.builder()
                .title("How often do you use your smartphone throughout the day?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionTH11, answerOptionTH12, answerOptionTH13, answerOptionTH14))
                .build();
        final Question questionTH2 = Question.builder()
                .title("What apps do you consider most important on your smartphone?")
                .type(QuestionType.MULTIPLE_CHOICE)
                .answerOptions(List.of(answerOptionTH21, answerOptionTH22, answerOptionTH23, answerOptionTH24))
                .build();
        final Question questionTH3 = Question.builder()
                .title("How do you feel about using voice assistants (e.g., Siri, Google Assistant)?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionTH31, answerOptionTH32, answerOptionTH33))
                .build();
        final Question questionTH4 = Question.builder()
                .title("How often do you use online shopping platforms (e.g., Amazon, eBay, AliExpress)?")
                .type(QuestionType.SINGLE_CHOICE)
                .answerOptions(List.of(answerOptionTH41, answerOptionTH42, answerOptionTH43, answerOptionTH44))
                .build();
        
        final Survey surveyTH = Survey.builder().title("Technological Habits?").questions(List.of(questionTH1, questionTH2, questionTH3, questionTH4)).author(user).build();
        
        userRepository.saveAll(List.of(user, user1, user2, user3, admin));
        surveyRepository.saveAll(List.of(surveyTH, surveyAAM, surveyPP, surveyCSN));
        log.info("Test data generation completed.");
    }
}