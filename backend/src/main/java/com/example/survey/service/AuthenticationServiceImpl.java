package com.example.survey.service;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.example.survey.common.ApplicationConstants;
import com.example.survey.mapper.UserMapper;
import com.example.survey.model.Role;
import com.example.survey.model.dto.AuthenticationRequest;
import com.example.survey.model.dto.AuthenticationResponse;
import com.example.survey.security.jwt.AuthenticatedUser;
import com.example.survey.security.jwt.JwtService;
import jakarta.annotation.Nonnull;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthenticationServiceImpl implements AuthenticationService {
    
    @Value("${spring.security.jwt.access-cookie-name}")
    String accessTokenCookieName;
    
    @Value("${spring.security.jwt.refresh-cookie-name}")
    String refreshTokenCookieName;
    
    @Value("${spring.security.jwt.access-cookie-lifetime}")
    long accessTokenCookieLifetime;
    
    @Value("${spring.security.jwt.refresh-cookie-lifetime}")
    long refreshTokenCookieLifetime;
    
    final UserDetailsService userDetailsService;
    final AuthenticationManager authenticationManager;
    final CookieService cookieService;
    final JwtService jwtService;
    final UserMapper userMapper;
    
    @Override
    public AuthenticationResponse authenticate(@Nonnull final AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(), authenticationRequest.getPassword()));
        
        final var user = (AuthenticatedUser) userDetailsService.loadUserByUsername(
                authenticationRequest.getEmail());
        
        final var cookieAccessToken = createAccessTokenCookie(
                user.getEmail(), user.getRole());
        
        final var cookieRefreshToken = createRefreshTokenCookie(user.getEmail());
        
        return AuthenticationResponse.builder()
                .cookieAccessToken(cookieAccessToken)
                .cookieRefreshToken(cookieRefreshToken)
                .authenticatedUser(userMapper.toAuthenticatedUserResponse(user)).build();
    }
    
    @Override
    public AuthenticationResponse tokenRefresh(@Nonnull final String refreshToken) {
        final var user = (AuthenticatedUser) userDetailsService.loadUserByUsername(
                jwtService.extractSubject(refreshToken));
        
        if (!jwtService.validateRefreshToken(refreshToken, user.getUsername())) {
            throw new JWTVerificationException("RefreshToken is invalid or expired");
        }
        
        return AuthenticationResponse.builder()
                .authenticatedUser(userMapper.toAuthenticatedUserResponse(user))
                .cookieAccessToken(createAccessTokenCookie(user.getEmail(), user.getRole()))
                .cookieRefreshToken(createRefreshTokenCookie(user.getEmail()))
                .build();
    }
    
    @Override
    public AuthenticationResponse logout() {
        SecurityContextHolder.clearContext();
        var accessTokenCookie = cookieService.deleteCookie(accessTokenCookieName,
                ApplicationConstants.Web.CookiePaths.ACCESS_TOKEN);
        var refreshTokenCookie = cookieService.deleteCookie(refreshTokenCookieName,
                ApplicationConstants.Web.CookiePaths.REFRESH_TOKEN);
        
        return AuthenticationResponse.builder()
                .cookieAccessToken(accessTokenCookie)
                .cookieRefreshToken(refreshTokenCookie)
                .build();
    }
    
    public AuthenticatedUser getAuthenticatedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        if (authentication == null || !(authentication.getPrincipal() instanceof AuthenticatedUser authenticatedUser)) {
            throw new AuthenticationCredentialsNotFoundException(
                    "Authentication is required to get current user email");
        }
        
        return authenticatedUser;
    }
    
    /**
     * Create a cookie response with access token.
     *
     * @param email the user email to include in the token
     * @param role  user authority to include in the token
     * @return cookie response with access token
     */
    private ResponseCookie createAccessTokenCookie(@Nonnull final String email,
                                                   @Nonnull final Role role) {
        
        return cookieService.createCookie(accessTokenCookieName,
                jwtService.createAccessToken(email, role),
                ApplicationConstants.Web.CookiePaths.ACCESS_TOKEN, accessTokenCookieLifetime);
    }
    
    /**
     * Create a cookie response with refresh token.
     *
     * @param email the user email to include in the token
     * @return cookie response with refresh token
     */
    private ResponseCookie createRefreshTokenCookie(@Nonnull final String email) {
        
        return cookieService.createCookie(refreshTokenCookieName,
                jwtService.createRefreshToken(email),
                ApplicationConstants.Web.CookiePaths.REFRESH_TOKEN, refreshTokenCookieLifetime);
    }
}
