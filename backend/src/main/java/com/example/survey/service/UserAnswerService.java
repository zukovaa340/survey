package com.example.survey.service;

import com.example.survey.model.AnswerOption;
import com.example.survey.model.UserAnswer;
import com.example.survey.repository.AnswerOptionRepository;
import com.example.survey.repository.UserAnswerRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
public class UserAnswerService {
    private final UserAnswerRepository userAnswerRepository;
    private final QuestionService questionService;
    private final AnswerOptionRepository answerOptionRepository;
    
    public List<UserAnswer> getAllBySurveyId(Integer surveyId) {
        return userAnswerRepository.findAllByQuestionSurveyId(surveyId);
    }

    public List<UserAnswer> getAllByQuestionId(Integer questionId) {
        return userAnswerRepository.findAllByQuestionId(questionId);
    }
    
    public UserAnswer save(Integer questionId, UserAnswer userAnswer) {
        var answerOptions = new HashSet<AnswerOption>();
        var question = questionService.gerReferenceById(questionId);
        userAnswer.getAnswerOptions().stream().map(ao -> answerOptionRepository.getReferenceById(ao.getId()))
                .forEach(answerOptions::add);
        question.addUserAnswer(userAnswer);

        userAnswer.setAnswerOptions(answerOptions);
        
        return userAnswerRepository.save(userAnswer);
    }
    
    public List<UserAnswer> save(List<UserAnswer> userAnswers) {
        var userAnswersToSave = userAnswers.stream().map(userAnswer -> {
            if (userAnswer.getQuestion().getId() != null) {
                userAnswer.setQuestion(questionService.gerReferenceById(userAnswer.getQuestion().getId()));
            }
            if (!userAnswer.getAnswerOptions().isEmpty()) {
                var answerOptions = new HashSet<AnswerOption>();
                userAnswer.getAnswerOptions().stream().map(ao -> answerOptionRepository.getReferenceById(ao.getId()))
                        .forEach(answerOptions::add);
                userAnswer.setAnswerOptions(answerOptions);
            }
            
            
            return userAnswer;
        }).toList();
        
        return userAnswerRepository.saveAll(userAnswersToSave);
    }
    
    public void deleteById(Integer id) {
        userAnswerRepository.deleteById(id);
    }
    
    public UserAnswer update(Integer surveyId, Integer questionId, UserAnswer userAnswer) {
        return userAnswerRepository.save(userAnswer);
    }
}