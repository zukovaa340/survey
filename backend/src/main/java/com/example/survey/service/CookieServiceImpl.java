package com.example.survey.service;

import jakarta.annotation.Nonnull;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.server.Cookie;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

@Service
@Slf4j
public class CookieServiceImpl implements CookieService {
    
    @Override
    public String getCookieValueFromRequest(@Nonnull final HttpServletRequest request,
                                            @Nonnull final String name) {
        final var cookie = WebUtils.getCookie(request, name);
        
        if (cookie == null) {
            log.info("Unable resolve token" +
                    " from the cookies. It can be null or invalid name.");
            return null;
        }
        
        return cookie.getValue();
    }
    
    @Override
    public ResponseCookie createCookie(@Nonnull final String name,
                                       @Nonnull final String value,
                                       @Nonnull final String path,
                                       @Nonnull final Long lifetimeInSeconds) {
        return ResponseCookie
                .from(name, value)
                .path(path)
                .maxAge(lifetimeInSeconds)
                .httpOnly(true)
                .secure(false)
                .sameSite(Cookie.SameSite.STRICT.name())
                .build();
    }
    
    @Override
    public ResponseCookie deleteCookie(@Nonnull final String name,
                                       @Nonnull final String path) {
        return createCookie(name, "", path, 0L);
    }
}
