package com.example.survey.service;


import com.example.survey.exception.NotFoundException;
import com.example.survey.exception.UserAlreadyExistException;
import com.example.survey.model.Role;
import com.example.survey.model.Status;
import com.example.survey.model.User;
import com.example.survey.repository.UserRepository;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    
    public User addUser(@Nonnull final User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        return userRepository.save(user);
    }
    
    public User createPlainUser(@Nonnull final User user, String confirmationPassword) {
        if (!Objects.equals(user.getPassword(), confirmationPassword)) {
            throw new ConstraintViolationException("Passwords are not equal", null);
        }
        if (user.getStatus() == null) {
            user.setStatus(Status.ACTIVE);
        }
        if (user.getRole() == null) {
            user.setRole(Role.USER);
        }
        
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }
    
    public Page<User> findAll(Pageable pageable) {
        
        return userRepository.findAll(pageable);
    }
    
    public List<User> findAll() {
        return userRepository.findAll();
    }
    
    @Transactional
    public User updateUser(@Nonnull final Long id, @Nonnull final User userFromRequest) {
        User userFromDb = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Survey is not found"));
        
        if (validateEmailUpdate(userFromDb, userFromRequest.getEmail())) {
            userFromDb.setEmail(userFromRequest.getEmail());
        }
        
        userFromDb.setFirstName(userFromRequest.getFirstName());
        userFromDb.setLastName(userFromRequest.getLastName());
        userFromDb.setPhone(userFromRequest.getPhone());
        userFromDb.setRole(userFromRequest.getRole());
        userFromDb.setStatus(userFromRequest.getStatus() == null ? Status.ACTIVE : userFromRequest.getStatus());
        
        return userRepository.save(userFromDb);
    }
    
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("User is not found"));
    }
    
    public User getReferenceById(Long id) {
        return userRepository.getReferenceById(id);
    }
    
    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow();
    }
    
    public void deleteUserById(Long id) {
        getById(id);
        userRepository.deleteById(id);
    }
    
    public Boolean isEmailAvailable(String email) {
        return !userRepository.existsByEmail(email);
    }
    
    public boolean validateEmailUpdate(@Nonnull User user, @Nullable String email) {
        if (email == null) {
            throw new ConstraintViolationException("User's email can't be null", null);
        }
        if (user.getEmail().equals(email) || isEmailAvailable(email)) {
            return true;
        } else {
            throw new UserAlreadyExistException("User with this email already exists");
        }
    }
    
}
