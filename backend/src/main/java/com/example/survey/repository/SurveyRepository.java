package com.example.survey.repository;

import com.example.survey.model.Survey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Integer> {
    Page<Survey> findAllByTitleContainingIgnoreCase(String title, Pageable pageable);

    @Query("SELECT MAX(q.count) FROM (SELECT count(q.id) as count FROM Survey s JOIN s.questions q GROUP BY s.id) as q")
    Integer findMaxQuestionsInSurveys();
}
