package com.example.survey.repository;

import com.example.survey.model.UserAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAnswerRepository extends JpaRepository<UserAnswer, Integer> {
    
    List<UserAnswer> findAllByQuestionSurveyId(Integer surveyId);
    List<UserAnswer> findAllByQuestionId(Integer surveyId);
    
}
